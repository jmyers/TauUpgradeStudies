#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include "TH1.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TH2.h"
#include "TGraphErrors.h"
#include "TNtuple.h"
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <iostream>
#include "TEventList.h"
#include "TInterpreter.h"
#include "TLorentzVector.h"
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class vector<TLorentzVector>;
#endif
#include "TVector2.h"
#include "TVector3.h"
#include "TLatex.h"
#include "TLegend.h"
#include <TStyle.h>
#include <stdlib.h>
#include <fstream>
#include "TH1F.h"
#include "TF1.h"
#include "TObjArray.h"
#include "TLine.h"
#include <iomanip>
#include "TSystem.h"
#include "MyAnalysis/caloD3PD.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "MyAnalysis/myxAODAnalysis.h"
#include "xAODRootAccess/tools/Message.h"
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
   do {                                                     \
      if( ! EXP.isSuccess() ) {                             \
         Error( CONTEXT,                                    \
                XAOD_MESSAGE( "Failed to execute: %s" ),    \
                #EXP );                                     \
         return EL::StatusCode::FAILURE;                    \
      }                                                     \
   } while( false )

#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"
//#include "TauAnalysisTools/TauTruthMatchingTool.h"
using namespace std;

float wrapPhi( float phi ) {
        static const float M_2PI = 2.*TMath::Pi();
        while (phi> M_PI) phi -= M_2PI;
        while (phi<-M_PI) phi += M_2PI;
        return phi;
}
float deltaR (float eta1, float eta2, float phi1, float phi2){
        float dEta = eta1 - eta2;
        float dPhi=wrapPhi(phi1-phi2);
        return sqrt(dEta*dEta + dPhi*dPhi);
}

void examineTruthTau(const xAOD::TruthParticle& xTruthParticle){
    //Put in Truth decoding stuff here
    xTruthParticle.auxdecor<bool>("IsLeptonicTau") = false;

    TLorentzVector VisSumTLV;
    xTruthParticle.auxdecor<double>("pt_vis") = 0;
    xTruthParticle.auxdecor<double>("eta_vis") = 0;
    xTruthParticle.auxdecor<double>("phi_vis") = 0;
    xTruthParticle.auxdecor<double>("m_vis") = 0;
    xTruthParticle.auxdecor<int>("childChargeSum") = 0;
    xTruthParticle.auxdecor<int>("nTracks") = 0;
    xTruthParticle.auxdecor<bool>("neutrals") = false;
    xTruthParticle.auxdecor<int>("numChargedPions") = 0;
    xTruthParticle.auxdecor<int>("numNeutralPions") = 0;

    if(!xTruthParticle.hasDecayVtx()) return;

    const xAOD::TruthVertex* decayvtx = xTruthParticle.decayVtx();
    if(decayvtx)
    {
        const std::size_t nChildren = decayvtx->nOutgoingParticles();
        for ( std::size_t iChild = 0; iChild != nChildren; ++iChild )
        {
            const xAOD::TruthParticle * child = decayvtx->outgoingParticle(iChild);
            if((abs(child->pdgId()) == 12 || abs(child->pdgId()) == 14 || abs(child->pdgId()) == 16)) 
		{continue;}
            if(child->status()==3)
		{ continue;}
            if ((abs(child->pdgId()) == 11 || abs(child->pdgId()) == 13 || abs(child->pdgId()) == 15)) 
		{xTruthParticle.auxdecor<bool>("IsLeptonicTau") = true;}
	    if (abs(child->pdgId()) == 211)
		{xTruthParticle.auxdecor<int>("numChargedPions") = xTruthParticle.auxdecor<int>("numChargedPions") + 1;}
	    if (abs(child->pdgId()) == 111)
                {xTruthParticle.auxdecor<int>("numNeutralPions") = xTruthParticle.auxdecor<int>("numNeutralPions") + 1;}

            VisSumTLV += child->p4();
            xTruthParticle.auxdecor<int>("childChargeSum") += child->charge();
            xTruthParticle.auxdecor<int>("nTracks") += abs(child->charge());
            if(child->charge()==0) xTruthParticle.auxdecor<bool>("neutrals") = true;
        }
    }

    xTruthParticle.auxdecor<double>("pt_vis")  = VisSumTLV.Pt();
    xTruthParticle.auxdecor<double>("eta_vis") = VisSumTLV.Eta();
    xTruthParticle.auxdecor<double>("phi_vis") = VisSumTLV.Phi();
    xTruthParticle.auxdecor<double>("m_vis")   = VisSumTLV.M();
    if(xTruthParticle.auxdecor<int>("childChargeSum")!=xTruthParticle.charge() || xTruthParticle.auxdecor<int>("nTracks")%2==0)
      {
        Info("event loop", "strange tau: charge %i  and %i tracks",xTruthParticle.auxdecor<int>("childChargeSum"),xTruthParticle.auxdecor<int>("nTracks") );
        const std::size_t nChildren = decayvtx->nOutgoingParticles();
        for ( std::size_t iChild = 0; iChild != nChildren; ++iChild )
          {
            const xAOD::TruthParticle * child = decayvtx->outgoingParticle(iChild);
            Info("event loop", "  child pdgId %i, status %i, charge %f",child->pdgId(),child->status(),child->charge());
          }
      }

}

enum {
        e_l1,
        e_l2_1,e_l2_2,e_l2_3,e_l2_4,
        e_l3_1,e_l3_2,e_l3_3,e_l3_4,
        e_l4,
        e_tt_EM, e_tt_HAD, e_sc_EM,
        e_sc_isoEM, e_tt_isoEM, e_tt_isoHAD,
        e_sc_coreEM, e_tt_coreEM, e_tt_coreHAD,
        e_sc_coreL1, e_sc_coreL2, e_sc_coreL3, e_sc_coreL4,
        e_sc_isoL1, e_sc_isoL2inner, e_sc_isoL2outer, e_sc_isoL3inner, e_sc_isoL3outer, e_sc_isoL4,
        e_true,
        e_size
};


int Layer(int detCells){

  //int calo = -999;
  int layer =  -999;
  //bool isEM = true;
//--------------------------------------------------------------------------------
  if (detCells == 81 || detCells == 273 || detCells ==145)//front
	{layer = 1;}
	//{calo = 0; layer = 1;}
//--------------------------------------------------------------------------------
  else if (detCells == 97 || detCells == 289 || detCells ==161)//middle
	{layer = 2;}
	//{calo = 0; layer = 2;}
//--------------------------------------------------------------------------------
  else if (detCells == 65 || detCells == 257)//PreS
        {layer = 0;}
        //{calo = 0; layer = 0;}
//--------------------------------------------------------------------------------
  else if (detCells == 113 || detCells == 305)//Back
        {layer = 3;}
        //{calo = 0; layer = 3;}
//--------------------------------------------------------------------------------
  else if (detCells == 65544 || detCells == 131080)//Tile 0
        {layer = 0;}
        //{calo = 2; layer = 0; isEM = false;}
//--------------------------------------------------------------------------------
  else if (detCells == 2)//HEC
        {layer = 0;} 
        //{calo = 1; layer = 0; isEM = false;} 
//--------------------------------------------------------------------------------
  else if (detCells == 2052)//FCAL 0
        {layer = 0;} 
        //{calo = 3; layer = 0;} 
//--------------------------------------------------------------------------------
  else if (detCells == 4100 || detCells == 131080)//FCAL 1
        {layer = 1;}
        //{calo = 3; layer = 1; isEM = false;}
//-------------------------------------------------------------------------------- 
  else if (detCells == 6148)//FCAL 2
        {layer = 2;} 
        //{calo = 3; layer = 2;isEM = false;} 
//--------------------------------------------------------------------------------
  else if (detCells == 81928 || detCells == 147464)//Tile Layer D
        {layer = 1;} 
        //{calo = 2;*/ layer = 1; isEM = false;} 
//--------------------------------------------------------------------------------
  return layer;

}

void makeLUT(caloD3PD *my_calo, vector<vector<unsigned int>>& SC_Neighbor_Index){

	Long64_t ientry = my_calo->LoadTree(0);
	if(ientry < 0){cout<<"error"<<endl;}
	my_calo->fChain->GetEntry(0);
	TLorentzVector seed;
	TLorentzVector neigh;
	int count = 0;
	unsigned int nscells = my_calo->scells_n;
	//unsigned int ntt = my_calo->tt_n;
	for(unsigned int sc =0;sc < nscells;sc++){
		float eta = my_calo->scells_eta->at(sc);
		float phi = my_calo->scells_phi->at(sc);
		
		count = count +1;
		vector<unsigned int> SC_Index;
		for(unsigned int nbr = 0; nbr < nscells;nbr++){
			float eta2 = my_calo->scells_eta->at(nbr);
			float phi2 = my_calo->scells_phi->at(nbr);
			float pt = my_calo->scells_pt->at(nbr);
			float dEta = fabs(eta - eta2);
			float dPhi = fabs(TVector2::Phi_mpi_pi(phi-phi2));
			//define iso region
			if(dEta < 0.21 && dPhi < 0.21){
				SC_Index.push_back(nbr);
			}
		}
		
		SC_Neighbor_Index.push_back(SC_Index);
	}
}
vector<float>* m_true_pt;
vector<float>* m_fcore;
vector<float>* m_R_EM;
vector<float>* m_fcore_M;
vector<float>* m_R_EM_M;
vector<float>* clus_pt;
vector<float>* clus_pt_M;
vector<float>* m_true_eta;
vector<float>* m_true_phi;
vector<float>* m_seed_eta;
vector<float>* m_seed_phi;
vector<float>* m_seed_eta_M;
vector<float>* m_seed_phi_M;
vector<float>* m_core_pt;
vector<float>* m_iso_pt;
vector<float>* m_core_pt_M;
vector<float>* m_iso_pt_M;
vector<int>* m_decay;
vector<int>* nlayer2;
vector<int>* nlayer2_M;
int isoeta;
int isophi;
int coreeta;
int corephi; 
unsigned int bcId;
void CreateObj(){
	m_true_pt = new vector<float>;
        m_R_EM = new vector<float>;
        m_R_EM_M = new vector<float>;
        m_fcore = new vector<float>;
        m_fcore_M = new vector<float>;
	clus_pt = new vector<float>;
	clus_pt_M = new vector<float>;
	m_true_eta = new vector<float>;
	m_true_phi = new vector<float>;
	m_seed_eta = new vector<float>;
	m_seed_phi = new vector<float>;
	m_seed_eta_M = new vector<float>;
	m_seed_phi_M = new vector<float>;
	m_iso_pt = new vector<float>;
	m_core_pt = new vector<float>;
	m_iso_pt_M = new vector<float>;
	m_core_pt_M = new vector<float>;
	m_decay = new vector<int>;
	nlayer2 = new vector<int>;
	nlayer2_M = new vector<int>;
}
void CleanUp(){
	delete m_true_pt;
        delete m_R_EM;
        delete m_R_EM_M;
        delete m_fcore;
        delete m_fcore_M;
	delete clus_pt;
	delete clus_pt_M;
	delete m_true_eta;
	delete m_true_phi;
	delete m_seed_eta;
	delete m_seed_phi;
	delete m_seed_eta_M;
	delete m_seed_phi_M;
	delete m_iso_pt;
	delete m_core_pt;
	delete m_iso_pt_M;
	delete m_core_pt_M;
	delete m_decay;
	delete nlayer2;
	delete nlayer2_M;

}
void CleanupSeeds(vector<unsigned int>& super, caloD3PD* my_d3pd,vector<vector<unsigned int>> LUT){
        vector<unsigned int> overlap;
        vector<unsigned int> neigh;

        for(unsigned int iter = 0; iter < super.size();iter++){
                bool flag = true;
                unsigned int nsz = neigh.size();
                //cout<<seeds[iter]<<endl;
                if(iter > 0){
                        for(unsigned int nbr = 0;nbr<nsz;nbr++){
                                if(super[iter] == neigh[nbr]){overlap.insert(overlap.begin(),iter);flag = false; break;}
                        }
                }
                if(!flag){continue;}
                for(unsigned int dum = 0; dum < LUT[super[iter]].size(); dum++){
                        if(Layer(my_d3pd->scells_DetCells->at(LUT[super[iter]][dum]))){
                                neigh.push_back(LUT[super[iter]][dum]);
                        }
                }

        }//finds duplicates

        for(unsigned int blah = 0; blah < overlap.size();blah++){
                super.erase(super.begin()+overlap[blah]);
        }//removes duplicates
}
float E_to_Et(float E, float eta){
        float Et = E * sin(2*atan(exp(eta)));
        return Et;
}

int main(int argc, char **argv){
	if(argc<2){
                std::cout<<"missing input parameters!"<<std::endl;
                std::cout<<"[1] input files"<<std::endl;
                std::cout<<"[2] truth?"<<std::endl;
                std::cout<<"[3] output name"<<std::endl;
                std::cout<<"[4] n Isolation Cells Eta"<<std::endl;
                std::cout<<"[5] n Isolation Cells Phi"<<std::endl;
                std::cout<<"[6] n Core Cells Eta"<<std::endl;
                std::cout<<"[7] n Core Cells Phi"<<std::endl;
                return 0;
        }
        const char* input_samples = argv[1];
        const char* output_name = "output.root";
        int isTruth(0);
        if(argc>2) isTruth = atoi(argv[2]);
        std::cout<<"isTruth? "<<isTruth<<std::endl;
        if(argc>3) output_name = argv[3];
        std::cout<<"output: "<<output_name<<std::endl;

	//RoI Configuration
	int n_eta_iso = 0;
        if(argc>4) n_eta_iso = atoi(argv[4]);
        int n_phi_iso = 0;
        if(argc>5) n_phi_iso = atoi(argv[5]);
        int n_eta_core = 0;
        if(argc>6) n_eta_core = atoi(argv[6]);
        int n_phi_core = 0;
        if(argc>7) n_phi_core = atoi(argv[7]);
        float acceptance_eta = 2.47;
        cout << "## RoI Configurations: ##" << endl;
        cout << "       n Iso Eta " << n_eta_iso << endl;
        cout << "       n Iso Phi " << n_phi_iso << endl;
        cout << "       n Core Eta " << n_eta_core << endl;
        cout << "       n Core Phi " << n_phi_core << endl;
        cout << "       Eta RoI acceptance " << acceptance_eta << endl;
	
	float seed_thresh = 1000.;
	float truth_thresh = -9.9E6;
	float scell_thresh = 750.;	
        /*float n_eta_iso  = 11;
        float n_phi_iso  = 3;
        float n_eta_core = 5;
        float n_phi_core = 1;
	*/
	//#############################################333
	//Need to fix for nphi == even

        float max_eta_iso = ((n_eta_iso - 1.)/2.)*0.025+0.005;
        float max_phi_iso = ((n_phi_iso - 1.)/2.)*0.1+0.01;
        float max_eta_core = ((n_eta_core - 1.)/2.)*0.025+0.005;
        float max_phi_core = ((n_phi_core - 1.)/2.)*0.1+0.01;
	
	if(n_phi_iso%2==0){max_phi_iso = (n_phi_iso/2.)*0.1+0.01;}
	if(n_phi_core%2==0){max_phi_core = (n_phi_core/2.)*0.1+0.01;}
	
	std::vector<std::string> fileList;
        ifstream myfile;
        std::string input(input_samples);
        if(input.find(".txt")!=std::string::npos){
                myfile.open(input_samples);
                if(!myfile.is_open()){ std::cout<<"cannot open input file"<<std::endl;
                        return 0;
                }
                char tmp[600];
                while(myfile.good()){
                        myfile.getline(tmp,600);
                        fileList.push_back(tmp);
                }
                for(unsigned int i=0;i<fileList.size();i++){
                        if(fileList.at(i).find(".root")==std::string::npos){ fileList.erase(fileList.begin()+i);
                                i--;}
                }
        }
        if(input.find(".root")!=std::string::npos){
                for (size_t i=0,n; i <= input.length(); i=n+1){
                        n = input.find_first_of(',',i);
                        if (n == std::string::npos) n = input.length();
                        std::string tmp = input.substr(i,n-i);
                        fileList.push_back(tmp);
                }
        }
	TFile* output =  new TFile(output_name,"RECREATE");
	TTree* mytree = new TTree("mytree","mytree");
	mytree->Branch("m_fcore",&m_fcore);
	mytree->Branch("m_R_EM",&m_R_EM);
	mytree->Branch("clus_pt",&clus_pt);
	mytree->Branch("isoeta",&isoeta);
	mytree->Branch("isophi",&isophi);
	mytree->Branch("coreeta",&coreeta);
	mytree->Branch("corephi",&corephi);
	mytree->Branch("m_core_pt",&m_core_pt);
	mytree->Branch("m_iso_pt",&m_iso_pt);
	mytree->Branch("nlayer2",&nlayer2);
	if(isTruth){
		mytree->Branch("m_seed_eta_M",&m_seed_eta_M);
		mytree->Branch("m_seed_phi_M",&m_seed_phi_M);
		mytree->Branch("clus_pt_M",&clus_pt_M);
		mytree->Branch("m_R_EM_M",&m_R_EM_M);
		mytree->Branch("m_fcore_M",&m_fcore_M);
		mytree->Branch("m_true_eta",&m_true_eta);
		mytree->Branch("m_true_pt",&m_true_pt);
		mytree->Branch("m_true_phi",&m_true_phi);
		mytree->Branch("m_decay",&m_decay);
		mytree->Branch("nlayer2_M",&nlayer2_M);
		mytree->Branch("m_iso_pt_M",&m_iso_pt_M);
		mytree->Branch("m_core_pt_M",&m_core_pt_M);
	}
	mytree->Branch("m_seed_eta",&m_seed_eta);
	mytree->Branch("m_seed_phi",&m_seed_phi);
	mytree->Branch("bcId",&bcId);
        TChain *d3pdChain = new TChain("caloD3PD");
        TChain *xaodChain = new TChain("CollectionTree");
        for(unsigned int file=0;file<fileList.size();file++ ){
                d3pdChain->Add(fileList.at(file).c_str());
                xaodChain->Add(fileList.at(file).c_str());
        }

	caloD3PD*  my_calo = new caloD3PD(d3pdChain);
        my_calo->fChain->SetBranchStatus("*",0);
        my_calo->fChain->SetBranchStatus("EventNumber",1);
        my_calo->fChain->SetBranchStatus("scells_n",1);
        my_calo->fChain->SetBranchStatus("scells_E",1);
        my_calo->fChain->SetBranchStatus("scells_pt",1);
        my_calo->fChain->SetBranchStatus("scells_eta",1);
        my_calo->fChain->SetBranchStatus("scells_phi",1);
        my_calo->fChain->SetBranchStatus("scells_xCells",1);
        my_calo->fChain->SetBranchStatus("scells_yCells",1);
        my_calo->fChain->SetBranchStatus("scells_zCells",1);
	my_calo->fChain->SetBranchStatus("scells_DetCells",1);
        my_calo->fChain->SetBranchStatus("tt_n",1);
        my_calo->fChain->SetBranchStatus("tt_em_E",1);
        my_calo->fChain->SetBranchStatus("tt_had_E",1);
        my_calo->fChain->SetBranchStatus("tt_eta",1);
        my_calo->fChain->SetBranchStatus("tt_phi",1);
	my_calo->fChain->SetBranchStatus("bcid",1);
        xAOD::Init();
        xAOD::TEvent* my_truth = new  xAOD::TEvent();
        EL_RETURN_CHECK( "readFrom(xaodChain)", my_truth->readFrom(xaodChain) );

        Long64_t nentries = my_calo->fChain->GetEntriesFast();
        Long64_t nentries_calo = my_calo->fChain->GetEntries();
        Long64_t nentries_truth = my_truth->getEntries();
	cout<<"calo entries: "<< nentries_calo << ", truth entries: "<< nentries_truth << endl;
        if(nentries_calo!=nentries_truth) { cout << "different entries" << endl; return 0;};


	vector<vector<unsigned int>> SLUT;
	vector<vector<unsigned int>> TLUT;
	cout<<"Creating LUT"<<endl;
	makeLUT(my_calo,SLUT);
	cout<<"Finished Creating LUT"<<endl;
        Long64_t nbytes = 0, nb = 0;
        for (Long64_t jentry=0; jentry<nentries;jentry++) {
		CreateObj();
                Long64_t ientry = my_calo->LoadTree(jentry);
                if (ientry < 0) break;
                nb = my_calo->fChain->GetEntry(jentry);   nbytes += nb;
                //std::cout<< my_calo->EventNumber << endl;
                my_truth->getEntry(jentry);
                if(jentry%200==0) cout<< "Processing event "<<jentry<<endl;
		//if(jentry > 0){break;}//Run over 1 event
		//*******************************************************************
		//Get Meta/bcid
		bcId = my_calo->bcid;
		isoeta = n_eta_iso;
		isophi = n_phi_iso;
		coreeta = n_eta_core;
		corephi = n_phi_core;
		//
		//
		//Grab Truth info
		//
		vector<TLorentzVector> true_vistau;
		vector<int> true_nprong;
		vector<float> true_pt;
		vector<bool> true_neutrals;
		//vector<int> true_child;	
		const xAOD::TruthParticleContainer *truthContainer = 0;
		if(isTruth){	
			EL_RETURN_CHECK("event loop",my_truth->retrieve( truthContainer, "TruthParticles" ));
			xAOD::TruthParticleContainer::const_iterator truthItr,truth_cont_end = truthContainer->end();
			for (truthItr=truthContainer->begin(); truthItr != truth_cont_end; ++truthItr){
				if(abs((*truthItr)->pdgId()) == 15  && (*truthItr)->status() == 2){
					examineTruthTau(**truthItr);
					double pt  = (*truthItr)->auxdata<double>("pt_vis");
					double eta = (*truthItr)->auxdata<double>("eta_vis");
					double phi = (*truthItr)->auxdata<double>("phi_vis");
					double m   = (*truthItr)->auxdata<double>("m_vis");
					bool lep = (*truthItr)->auxdata<bool>("IsLeptonicTau");
					int ntracks = (*truthItr)->auxdata<int>("nTracks");
					bool neutral = (*truthItr)->auxdata<bool>("neutrals");
					int pi0 = (*truthItr)->auxdata<int>("numNeutralPions");
					int pi = (*truthItr)->auxdata<int>("numChargedPions");
					//int nchild = (*truthItr)->auxdata<int>("nChildren");
					/*for(unsigned int childItr = 0; childItr < (*truthItr)->nChildren(); childItr++){
						cout<<(*truthItr)->child(childItr)->pdgId()<<endl;
					}*/
					if(pt < truth_thresh || lep || fabs(eta) > acceptance_eta ) continue;
					TLorentzVector TruthTauTLV;
					int decay = -999;
					TruthTauTLV.SetPtEtaPhiM(pt,eta,phi,m);
					true_vistau.push_back(TruthTauTLV);
					true_nprong.push_back(ntracks);
					true_pt.push_back(TruthTauTLV.Pt());
					true_neutrals.push_back(neutral);
					m_true_pt->push_back(pt);
					m_true_eta->push_back(eta);
					m_true_phi->push_back(phi);
					//Tag for decay type 
					//0 = single pi
					//1 = pi pi0
					//2 = 3pi
					//-999 = other 
					if((pi == 1) && (pi0 == 0)){
						decay = 0;
					}
					if((pi == 1) && (pi0 > 0)){
						decay = 1;
					}
					if(pi == 3){
						decay = 2;
					}
					m_decay->push_back(decay);
					//true_child.push_back(nchild);
				}
			}// loop in truth
		}	
		//*************************************************************
		//Grab scell and TT info and put into vectors
		//
		
		unsigned int nscells = my_calo->scells_n;
		vector<float> seeds_pt;
		vector<unsigned int> seeds;

		for(unsigned int sc = 0;sc < nscells; sc++){
			float m_sc_eta = my_calo->scells_eta->at(sc);
			if(fabs(m_sc_eta) > acceptance_eta){continue;}
                        float m_sc_phi = my_calo->scells_phi->at(sc);
                        float m_sc_Et = my_calo->scells_pt->at(sc);//used to be E
			float m_sc_detcells = my_calo->scells_DetCells->at(sc);
                        float m_sc_layer = Layer(m_sc_detcells);
			if(m_sc_layer == 2){
				if(m_sc_Et > seed_thresh){
					if(seeds.size()==0 && fabs(m_sc_eta)<2.375){seeds.push_back(sc);seeds_pt.push_back(m_sc_Et);continue;}
					if(seeds.size()!=0 && fabs(m_sc_eta) < 2.375){
						for(unsigned int spot = 0; spot < seeds.size(); spot++){
							if(m_sc_Et > seeds_pt[spot]){seeds.insert(seeds.begin()+spot,sc);seeds_pt.insert(seeds_pt.begin()+spot,m_sc_Et);break;}	
							if(spot == (seeds.size()-1)){seeds.push_back(sc);seeds_pt.push_back(m_sc_Et);break;}
						}
					}
					//if(seeds.size()>9){break;}
				}
			}//only keeping layer 2
			
		}//read in scell info
		//******************************************************************************************************************
		//Remove seeds that are nieghbors of higher pt seeds
		CleanupSeeds(seeds,my_calo,SLUT);
		//******************************************************************************************************************
		//Match seeds to true taus	
		vector<unsigned int> matched_seeds;
		if(isTruth){
			for(unsigned int tru = 0;tru < m_true_eta->size();tru++){
				for(unsigned int scsd = 0; scsd<seeds.size();scsd++){
					float dEta = m_true_eta->at(tru) - my_calo->scells_eta->at(seeds[scsd]);
					float dPhi = TVector2::Phi_mpi_pi(my_calo->scells_phi->at(seeds[scsd]) - m_true_phi->at(tru));
					float delR = sqrt(pow(dEta,2) + pow(dPhi,2));
					if(delR < 0.2){
						matched_seeds.push_back(seeds[scsd]);
					}
				}
			}//match seeds
		}
		//*******************************************************************************************************************
		//calculate the Tau cluster variable for both matched and unmatched seeds
	
		for(unsigned int scsd = 0; scsd < seeds.size();scsd++){
			float tau_clus = 0.;
			int nl2 = 0;
			m_seed_eta->push_back(my_calo->scells_eta->at(seeds[scsd]));
			m_seed_phi->push_back(my_calo->scells_phi->at(seeds[scsd]));

			//float pt_blw = 0., pt_abv = 0.;
			/*for(unsigned int scnbr = 0; scnbr < SLUT[seeds[scsd]].size(); scnbr++){
				float layer = Layer(my_calo->scells_DetCells->at(SLUT[seeds[scsd]][scnbr]));
				if(layer != 2){continue;}
				float dEta = (my_calo->scells_eta->at(seeds[scsd]) - my_calo->scells_eta->at(SLUT[seeds[scsd]][scnbr]));
                                float dPhi = (TVector2::Phi_mpi_pi(my_calo->scells_phi->at(seeds[scsd])-my_calo->scells_phi->at(SLUT[seeds[scsd]][scnbr])));
				float Et = my_calo->scells_pt->at(SLUT[seeds[scsd]][scnbr]);
				if(layer == 2 && dPhi < 0.101 && dPhi > 0. && fabs(dEta) < 0.01){
                                        pt_blw = Et;
                                }
                                if(layer == 2 && dPhi > -0.101 && dPhi < 0. && fabs(dEta) < 0.01){
                                        pt_abv = Et;
                                }
			}*/
			for(unsigned int scnbr = 0; scnbr < SLUT[seeds[scsd]].size(); scnbr++){
				int layer = Layer(my_calo->scells_DetCells->at(SLUT[seeds[scsd]][scnbr]));
				float dEta = (my_calo->scells_eta->at(seeds[scsd]) - my_calo->scells_eta->at(SLUT[seeds[scsd]][scnbr]));
                                float dPhi = (TVector2::Phi_mpi_pi(my_calo->scells_phi->at(seeds[scsd])-my_calo->scells_phi->at(SLUT[seeds[scsd]][scnbr])));
				float Et = my_calo->scells_pt->at(SLUT[seeds[scsd]][scnbr]);
                                /*if(pt_abv > pt_blw){
                                        if((layer == 1 || layer == 2) && dPhi < 0.101 && dPhi > 0. && fabs(dEta) < 0.06){
                                                tau_clus += Et;
					}
				}
				else{
                                        if((layer == 1 || layer == 2) && dPhi > -0.101 && dPhi < 0. && fabs(dEta) < 0.06){
                                                tau_clus += Et;
                                        }
                                }*/
				if(layer == 2 && fabs(Et) < scell_thresh){
					continue;
				}
				if(fabs(dEta) < 0.055 && fabs(dPhi) < 0.11){
					tau_clus += Et;
					if(layer == 2){
						nl2 = nl2 + 1;
					}
				}
			}
			for(int ttnbr = 0; ttnbr < my_calo->tt_n; ttnbr++){
				float dEta = (my_calo->scells_eta->at(seeds[scsd]) - my_calo->tt_eta->at(ttnbr));
				float dPhi = (TVector2::Phi_mpi_pi(my_calo->scells_phi->at(seeds[scsd])-my_calo->tt_phi->at(ttnbr)));
				float Et_had = E_to_Et(my_calo->tt_had_E->at(ttnbr),my_calo->tt_eta->at(ttnbr));
                                if(fabs(dPhi) < 0.15 && fabs(dEta)< 0.15){
                                	tau_clus += Et_had;
                        	}
                        }

				clus_pt->push_back(tau_clus);
				nlayer2->push_back(nl2);
		}//TC for unmatched
		if(isTruth){
			for(unsigned int scsd = 0; scsd < matched_seeds.size();scsd++){
				float tau_clus_M = 0.;
				int nl2_M = 0;
				m_seed_eta_M->push_back(my_calo->scells_eta->at(matched_seeds[scsd]));
				m_seed_phi_M->push_back(my_calo->scells_phi->at(matched_seeds[scsd]));

				//float pt_blw = 0., pt_abv = 0.;
				/*for(unsigned int scnbr = 0; scnbr < SLUT[matched_seeds[scsd]].size(); scnbr++){
					float layer = Layer(my_calo->scells_DetCells->at(SLUT[matched_seeds[scsd]][scnbr]));
					if(layer != 2){continue;}
					float dEta = (my_calo->scells_eta->at(matched_seeds[scsd]) - my_calo->scells_eta->at(SLUT[matched_seeds[scsd]][scnbr]));
					float dPhi = (TVector2::Phi_mpi_pi(my_calo->scells_phi->at(matched_seeds[scsd])-my_calo->scells_phi->at(SLUT[matched_seeds[scsd]][scnbr])));
					float Et = my_calo->scells_pt->at(SLUT[matched_seeds[scsd]][scnbr]);
					if(layer == 2 && dPhi < 0.101 && dPhi > 0. && fabs(dEta) < 0.01){
						pt_blw = Et;
					}
					if(layer == 2 && dPhi > -0.101 && dPhi < 0. && fabs(dEta) < 0.01){
						pt_abv = Et;
					}
				}*/
				for(unsigned int scnbr = 0; scnbr < SLUT[matched_seeds[scsd]].size(); scnbr++){
					int layer = Layer(my_calo->scells_DetCells->at(SLUT[matched_seeds[scsd]][scnbr]));
					float dEta = (my_calo->scells_eta->at(matched_seeds[scsd]) - my_calo->scells_eta->at(SLUT[matched_seeds[scsd]][scnbr]));
					float dPhi = (TVector2::Phi_mpi_pi(my_calo->scells_phi->at(matched_seeds[scsd])-my_calo->scells_phi->at(SLUT[matched_seeds[scsd]][scnbr])));
					float Et = my_calo->scells_pt->at(SLUT[matched_seeds[scsd]][scnbr]);
					/*if(pt_abv > pt_blw){
						if((layer == 2 || layer == 1) && dPhi < 0.101 && dPhi > 0. && fabs(dEta) < 0.06){
							tau_clus_M += Et;
						}
						if((layer == 0 || layer == 3) && dPhi < 0.101 && dPhi > 0. && fabs(dEta) < 0.051){
							tau_clus_M += Et;
						}
					}
					//if(pt_abv < pt_blw){
					else{
						if((layer == 2 || layer == 1) && dPhi > -0.101 && dPhi < 0. && fabs(dEta) < 0.06){
							tau_clus_M += Et;
						}
						if((layer == 0 || layer == 3) && dPhi > -0.101 && dPhi < 0. && fabs(dEta) < 0.051){
							tau_clus_M += Et;
						}
					}*/
					if(layer == 2 && fabs(Et) < scell_thresh){
						continue;
					}
					if(fabs(dEta) < 0.055 && fabs(dPhi) < 0.11){
                                        	tau_clus_M += Et;
                                		if(layer == 2){
							nl2_M = nl2_M +1;
						}
					}

				}
				for(int ttnbr = 0; ttnbr < my_calo->tt_n; ttnbr++){
                                        float dEta = (my_calo->scells_eta->at(matched_seeds[scsd]) - my_calo->tt_eta->at(ttnbr));
                                        float dPhi = (TVector2::Phi_mpi_pi(my_calo->scells_phi->at(matched_seeds[scsd])-my_calo->tt_phi->at(ttnbr)));
                                        float Et_had = E_to_Et(my_calo->tt_had_E->at(ttnbr),my_calo->tt_eta->at(ttnbr));
                                        if(fabs(dPhi) < 0.15 && fabs(dEta)< 0.15){
                                                tau_clus_M += Et_had;
                                        }
                                }

					clus_pt_M->push_back(tau_clus_M);
					nlayer2_M->push_back(nl2_M);
			}//TC for matched seeds
		}	
		//*******************************************************************************************************************
		//Create iso and core lists as well as calculate fcore
		for(unsigned int scsd = 0; scsd < seeds.size();scsd++){
			float pt_abv = 0.;
			float pt_blw = 0.;
			float core_pt = 0.;
			float iso_pt = 0.;	
			float fcore = 0.;
			float R_EM = 0.;
			float tot_pt = 0.;
			float weighted_pt = 0.;
			if(n_phi_core%2==0 || n_phi_iso%2==0){
				for(unsigned int scnbr = 0; scnbr < SLUT[seeds[scsd]].size(); scnbr++){
					int layer = Layer(my_calo->scells_DetCells->at(SLUT[seeds[scsd]][scnbr]));
					//calculate variables used for fcore
					float dEta = (my_calo->scells_eta->at(seeds[scsd]) - my_calo->scells_eta->at(SLUT[seeds[scsd]][scnbr]));
					float dPhi = (TVector2::Phi_mpi_pi(my_calo->scells_phi->at(seeds[scsd])-my_calo->scells_phi->at(SLUT[seeds[scsd]][scnbr])));
					float Et =  my_calo->scells_pt->at(SLUT[seeds[scsd]][scnbr]);
					if(layer != 2 || fabs(Et) < scell_thresh){continue;}
                                        if(layer == 2 && dPhi < 0.101 && dPhi > 0. && fabs(dEta) < 0.01){
                                                pt_blw = Et;
                                        }
                                        if(layer == 2 && dPhi > -0.101 && dPhi < 0. && fabs(dEta) < 0.01){
                                                pt_abv = Et;
                                        }
				}
			}
			for(unsigned int scnbr = 0; scnbr < SLUT[seeds[scsd]].size(); scnbr++){
				int layer = Layer(my_calo->scells_DetCells->at(SLUT[seeds[scsd]][scnbr]));
				//calculate variables used for fcore
				float dEta = (my_calo->scells_eta->at(seeds[scsd]) - my_calo->scells_eta->at(SLUT[seeds[scsd]][scnbr]));
				float dPhi = (TVector2::Phi_mpi_pi(my_calo->scells_phi->at(seeds[scsd])-my_calo->scells_phi->at(SLUT[seeds[scsd]][scnbr])));
				float Et =  my_calo->scells_pt->at(SLUT[seeds[scsd]][scnbr]);
				if(layer != 2 || fabs(Et) < scell_thresh){continue;}
				//calculate variables used for R_EM
                                /*if(fabs(dPhi) < max_phi_core && fab(dEta) < max_eta_core){
                                        weighted_pt += (my_calo->scells_E->at(SLUT[seeds[scsd]][scnbr]))*sqrt(pow(dEta,2)+pow(dPhi,2));
                                        tot_pt += my_calo->scells_E->at(SLUT[seeds[scsd]][scnbr]);
                                }*/
				if(n_phi_core%2==0){
					if(pt_abv >= pt_blw){
						if(fabs(dEta) < max_eta_core && dPhi < (max_phi_core - 0.1) && fabs(dPhi) < max_phi_core){
							core_pt += Et;
						}
					}
					else if(pt_blw > pt_abv){
						if(fabs(dEta) < max_eta_core && dPhi > (0.1 - max_phi_core) && fabs(dPhi) < max_phi_core){
							core_pt += Et;
						}
					}
					else{continue;}	
				}
				else{
					if(fabs(dPhi) < max_phi_core && fabs(dEta) < max_eta_core){
						core_pt += Et;
					}
				}
				if(n_phi_iso%2==0){
					if(pt_abv >= pt_blw){
						if(dPhi < (max_phi_iso - 0.1) && fabs(dEta) < max_eta_iso){
							iso_pt += Et;						}
					}
					if(pt_blw > pt_abv){
						if(dPhi > (0.1 - max_phi_iso) && fabs(dEta) < max_eta_iso){
							iso_pt += Et;
						}
					}
				}
				else{
					if(fabs(dPhi) < max_phi_iso && fabs(dEta) < max_eta_iso){
						iso_pt += Et;
					}
				}
			}	
			m_iso_pt->push_back(iso_pt);
			m_core_pt->push_back(core_pt);
			R_EM = weighted_pt/tot_pt;
			fcore = core_pt/iso_pt;
			m_R_EM->push_back(R_EM);
			m_fcore->push_back(fcore);
			
		}//create core and iso variables
		
		//###############################################################################################

                //Create iso and core lists as well as calculate fcore_M
		if(isTruth){
			for(unsigned int scsd = 0; scsd < matched_seeds.size();scsd++){
				float pt_abv = 0.;
				float pt_blw = 0.;
				float core_pt_M = 0.;
				float iso_pt_M = 0.;
				float fcore_M = 0.;
				float R_EM_M = 0.;
				float tot_pt_M = 0.;
				float weighted_pt_M = 0.;
				if(n_phi_core%2==0 || n_phi_iso%2==0){
					for(unsigned int scnbr = 0; scnbr < SLUT[matched_seeds[scsd]].size(); scnbr++){
						int layer = Layer(my_calo->scells_DetCells->at(SLUT[matched_seeds[scsd]][scnbr]));
						//calculate variables used for fcore_M
						float dEta = (my_calo->scells_eta->at(matched_seeds[scsd]) - my_calo->scells_eta->at(SLUT[matched_seeds[scsd]][scnbr]));
						float dPhi = (TVector2::Phi_mpi_pi(my_calo->scells_phi->at(matched_seeds[scsd])-my_calo->scells_phi->at(SLUT[matched_seeds[scsd]][scnbr])));
						float Et = my_calo->scells_pt->at(SLUT[matched_seeds[scsd]][scnbr]);	
						if(layer != 2 || fabs(Et) < scell_thresh){continue;}
						if(layer == 2 && dPhi < 0.101 && dPhi > 0. && fabs(dEta) < 0.01){
							pt_blw = Et;
						}
						if(layer == 2 && dPhi > -0.101 && dPhi < 0. && fabs(dEta) < 0.01){
							pt_abv = Et;
						}
					}
				}
				for(unsigned int scnbr = 0; scnbr < SLUT[matched_seeds[scsd]].size(); scnbr++){
					int layer = Layer(my_calo->scells_DetCells->at(SLUT[matched_seeds[scsd]][scnbr]));
					//calculate variables used for fcore_M
					float dEta = (my_calo->scells_eta->at(matched_seeds[scsd]) - my_calo->scells_eta->at(SLUT[matched_seeds[scsd]][scnbr]));
					float dPhi = (TVector2::Phi_mpi_pi(my_calo->scells_phi->at(matched_seeds[scsd])-my_calo->scells_phi->at(SLUT[matched_seeds[scsd]][scnbr])));
					float Et =  my_calo->scells_pt->at(SLUT[matched_seeds[scsd]][scnbr]);
					if(layer != 2 || fabs(Et) < scell_thresh){continue;}
					//calculate variables used for R_EM_M
					/*if(fabs(dPhi) < max_phi_core && fab(dEta) < max_eta_core){
						weighted_pt_M += (my_calo->scells_E->at(SLUT[matched_seeds[scsd]][scnbr]))*sqrt(pow(dEta,2)+pow(dPhi,2));
						tot_pt_M += my_calo->scells_E->at(SLUT[matched_seeds[scsd]][scnbr]);
					}*/
					if(n_phi_core%2==0){
						if(pt_abv >= pt_blw){
							if(fabs(dEta) < max_eta_core && dPhi < (max_phi_core - 0.1) && fabs(dPhi) < max_phi_core){
								core_pt_M += Et;
							}
						}
						else if(pt_blw > pt_abv){
							if(fabs(dEta) < max_eta_core && dPhi > (0.1 - max_phi_core) && fabs(dPhi) < max_phi_core){
								core_pt_M += Et;
							}
						}
						else{continue;}
					}
					else{
						if(fabs(dPhi) < max_phi_core && fabs(dEta) < max_eta_core){
							core_pt_M += Et;
						}
					}
					if(n_phi_iso%2==0){
						if(pt_abv >= pt_blw){
							if(dPhi < (max_phi_iso - 0.1) && fabs(dEta) < max_eta_iso){
								iso_pt_M += Et;
							}
						}
						if(pt_blw > pt_abv){
							if(dPhi > (0.1 - max_phi_iso) && fabs(dEta) < max_eta_iso){
								iso_pt_M += Et;
							}
						}
					}
					else{
						if(fabs(dPhi) < max_phi_iso && fabs(dEta) < max_eta_iso){
							iso_pt_M += Et;
						}
					}
				}
				R_EM_M = weighted_pt_M/tot_pt_M;
				fcore_M = core_pt_M/iso_pt_M;
				m_R_EM_M->push_back(R_EM_M);
				m_fcore_M->push_back(fcore_M);
				m_core_pt_M->push_back(core_pt_M);
				m_iso_pt_M->push_back(iso_pt_M);

			}//create core and iso variables
		}
		//********************************************************************
		/*vector<TLorentzVector> tt_list;
		unsigned int ntt = my_calo->tt_n;
		for(unsigned int tt = 0; tt < ntt; tt++){
			//cout << endl;
			float m_tt_eta = my_calo->tt_eta->at(tt);
			if(fabs(m_tt_eta) > acceptance_eta){continue;}
			float m_tt_phi = my_calo->tt_phi->at(tt);
			float m_tt_em_Et = my_calo->tt_em_E->at(tt);
			float m_tt_had_Et = my_calo->tt_had_E->at(tt);
			
			TLorentzVector ttTLV;
			ttTLV.SetPtEtaPhiM((m_tt_em_Et+m_tt_had_Et),m_tt_eta,m_tt_phi,0);
			tt_list.push_back(ttTLV);
		}//read in tt info
		*/
		output->cd();
		mytree->Fill();
		CleanUp();
		
	}
	SLUT.clear();
	output->cd();
	mytree->Write();
	
	output->Close();
	delete my_calo;
	delete my_truth;
	return 0;
}

