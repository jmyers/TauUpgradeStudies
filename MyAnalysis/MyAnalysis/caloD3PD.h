//////////////////////////////////////////////////////////
//// This class has been automatically generated on
//// Wed Oct 14 01:38:09 2015 by ROOT version 5.34/32
//// from TTree caloD3PD/caloD3PD
//// found on file: user.aagaard.mc15_13TeV.341124.ggH125_tautauhh_r7081.v5_EXT0/user.aagaard.6548391.EXT0._000043.NTUP.root
////////////////////////////////////////////////////////////
//

#ifndef caloD3PD_h
#define caloD3PD_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <stdlib.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

using namespace std;

// Fixed size dimensions of array or collections stored in the TTree if any.
class caloD3PD {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   
   // Declaration of leaf types
   UInt_t          RunNumber;
   ULong64_t       EventNumber;
   UInt_t          timestamp;
   UInt_t          timestamp_ns;
   UInt_t          lbn;
   UInt_t          bcid;
   UInt_t          detmask0;
   UInt_t          detmask1;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXing;
   UInt_t          mc_channel_number;
   ULong64_t       mc_event_number;
   Float_t         mc_event_weight;
   UInt_t          pixelFlags;
   UInt_t          sctFlags;
   UInt_t          trtFlags;
   UInt_t          larFlags;
   UInt_t          tileFlags;
   UInt_t          muonFlags;
   UInt_t          fwdFlags;
   UInt_t          coreFlags;
   UInt_t          backgroundFlags;
   UInt_t          lumiFlags;
   UInt_t          pixelError;
   UInt_t          sctError;
   UInt_t          trtError;
   UInt_t          larError;
   UInt_t          tileError;
   UInt_t          muonError;
   UInt_t          fwdError;
   UInt_t          coreError;
   Bool_t          isSimulation;
   Bool_t          isCalibration;
   Bool_t          isTestBeam;
   Int_t           lardigit_sc_n;
   vector<int>     *lardigit_sc_layer;
   vector<int>     *lardigit_sc_ieta;
   vector<int>     *lardigit_sc_calo;
   vector<int>     *lardigit_sc_iphi;
   vector<int>     *lardigit_sc_barrel_ec;
   vector<int>     *lardigit_sc_pos_neg;
   vector<int>     *lardigit_sc_FT;
   vector<int>     *lardigit_sc_slot;
   vector<int>     *lardigit_sc_channel;
   vector<int>     *lardigit_sc_gain;
   vector<vector<int> > *lardigit_sc_Samples;
   vector<unsigned int> *lardigit_sc_offlineId;
   Int_t           cc_n;
   vector<float>   *cc_E;
   vector<float>   *cc_pt;
   vector<float>   *cc_eta;
   vector<float>   *cc_phi;
   vector<int>     *cc_QCells;
   vector<int>     *cc_GainCells;
   vector<int>     *cc_DetCells;
   vector<float>   *cc_TimeCells;
   vector<float>   *cc_xCells;
   vector<float>   *cc_yCells;
   vector<float>   *cc_zCells;
   vector<int>     *cc_BadCells;
   vector<unsigned int> *cc_IdCells;
   vector<float>   *cc_Sigma;
   Int_t           scells_n;
   vector<float>   *scells_E;
   vector<float>   *scells_pt;
   vector<float>   *scells_eta;
   vector<float>   *scells_phi;
   vector<int>     *scells_QCells;
   vector<int>     *scells_GainCells;
   vector<int>     *scells_DetCells;
   vector<float>   *scells_TimeCells;
   vector<float>   *scells_xCells;
   vector<float>   *scells_yCells;
   vector<float>   *scells_zCells;
   vector<int>     *scells_BadCells;
   vector<unsigned int> *scells_IdCells;
   vector<float>   *scells_Sigma;
   Int_t           tt_n;
   vector<float>   *tt_em_E;
   vector<float>   *tt_had_E;
   vector<int>     *tt_em_peak;
   vector<int>     *tt_had_peak;
   vector<int>     *tt_em_adc_peak;
   vector<int>     *tt_had_adc_peak;
   vector<vector<int> > *tt_em_lut;
   vector<vector<int> > *tt_had_lut;
   vector<vector<int> > *tt_em_adc;
   vector<vector<int> > *tt_had_adc;
   vector<float>   *tt_eta;
   vector<float>   *tt_phi;
   
   // List of branches
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_timestamp;   //!
   TBranch        *b_timestamp_ns;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_detmask0;   //!
   TBranch        *b_detmask1;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_mc_channel_number;   //!
   TBranch        *b_mc_event_number;   //!
   TBranch        *b_mc_event_weight;   //!
   TBranch        *b_pixelFlags;   //!
   TBranch        *b_sctFlags;   //!
   TBranch        *b_trtFlags;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_tileFlags;   //!
   TBranch        *b_muonFlags;   //!
   TBranch        *b_fwdFlags;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_lumiFlags;   //!
   TBranch        *b_pixelError;   //!
   TBranch        *b_sctError;   //!
   TBranch        *b_trtError;   //!
   TBranch        *b_larError;   //!
   TBranch        *b_tileError;   //!
   TBranch        *b_muonError;   //!
   TBranch        *b_fwdError;   //!
   TBranch        *b_coreError;   //!
   TBranch        *b_isSimulation;   //!
   TBranch        *b_isCalibration;   //!
   TBranch        *b_isTestBeam;   //!
   TBranch        *b_lardigit_sc_n;   //!
   TBranch        *b_lardigit_sc_layer;   //!
   TBranch        *b_lardigit_sc_ieta;   //!
   TBranch        *b_lardigit_sc_calo;   //!
   TBranch        *b_lardigit_sc_iphi;   //!
   TBranch        *b_lardigit_sc_barrel_ec;   //!
   TBranch        *b_lardigit_sc_pos_neg;   //!
   TBranch        *b_lardigit_sc_FT;   //!
   TBranch        *b_lardigit_sc_slot;   //!
   TBranch        *b_lardigit_sc_channel;   //!
   TBranch        *b_lardigit_sc_gain;   //!
   TBranch        *b_lardigit_sc_Samples;   //!
   TBranch        *b_lardigit_sc_offlineId;   //!
   TBranch        *b_cc_n;   //!
   TBranch        *b_cc_E;   //!
   TBranch        *b_cc_pt;   //!
   TBranch        *b_cc_eta;   //!
   TBranch        *b_cc_phi;   //!
   TBranch        *b_cc_QCells;   //!
   TBranch        *b_cc_GainCells;   //!
   TBranch        *b_cc_DetCells;   //!
   TBranch        *b_cc_TimeCells;   //!
   TBranch        *b_cc_xCells;   //!
   TBranch        *b_cc_yCells;   //!
   TBranch        *b_cc_zCells;   //!
   TBranch        *b_cc_BadCells;   //!
   TBranch        *b_cc_IdCells;   //!
   TBranch        *b_cc_Sigma;   //!
   TBranch        *b_scells_n;   //!
   TBranch        *b_scells_E;   //!
   TBranch        *b_scells_pt;   //!
   TBranch        *b_scells_eta;   //!
   TBranch        *b_scells_phi;   //!
   TBranch        *b_scells_QCells;   //!
   TBranch        *b_scells_GainCells;   //!
   TBranch        *b_scells_DetCells;   //!
   TBranch        *b_scells_TimeCells;   //!
   TBranch        *b_scells_xCells;   //!
   TBranch        *b_scells_yCells;   //!
   TBranch        *b_scells_zCells;   //!
   TBranch        *b_scells_BadCells;   //!
   TBranch        *b_scells_IdCells;   //!
   TBranch        *b_scells_Sigma;   //!
   TBranch        *b_tt_n;   //!
   TBranch        *b_tt_em_E;   //!
   TBranch        *b_tt_had_E;   //!
   TBranch        *b_tt_em_peak;   //!
   TBranch        *b_tt_had_peak;   //!
   TBranch        *b_tt_em_adc_peak;   //!
   TBranch        *b_tt_had_adc_peak;   //!
   TBranch        *b_tt_em_lut;   //!
   TBranch        *b_tt_had_lut;   //!
   TBranch        *b_tt_em_adc;   //!
   TBranch        *b_tt_had_adc;   //!
   TBranch        *b_tt_eta;   //!
   TBranch        *b_tt_phi;   //!

   caloD3PD(TTree *tree=0);
   virtual ~caloD3PD();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef caloD3PD_cxx
caloD3PD::caloD3PD(TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// // used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("user.aagaard.mc15_13TeV.341124.ggH125_tautauhh_r7081.v5_EXT0/user.aagaard.6548391.EXT0._000043.NTUP.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("user.aagaard.mc15_13TeV.341124.ggH125_tautauhh_r7081.v5_EXT0/user.aagaard.6548391.EXT0._000043.NTUP.root");
      }
      f->GetObject("caloD3PD",tree);

   }
   Init(tree);
}

caloD3PD::~caloD3PD()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t caloD3PD::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t caloD3PD::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void caloD3PD::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).
   
   // Set object pointer
   lardigit_sc_layer = 0;
   lardigit_sc_ieta = 0;
   lardigit_sc_calo = 0;
   lardigit_sc_iphi = 0;
   lardigit_sc_barrel_ec = 0;
   lardigit_sc_pos_neg = 0;
   lardigit_sc_FT = 0;
   lardigit_sc_slot = 0;
   lardigit_sc_channel = 0;
   lardigit_sc_gain = 0;
   lardigit_sc_Samples = 0;
   lardigit_sc_offlineId = 0;
   cc_E = 0;
   cc_pt = 0;
   cc_eta = 0;
   cc_phi = 0;
   cc_QCells = 0;
   cc_GainCells = 0;
   cc_DetCells = 0;
   cc_TimeCells = 0;
   cc_xCells = 0;
   cc_yCells = 0;
   cc_zCells = 0;
   cc_BadCells = 0;
   cc_IdCells = 0;
   cc_Sigma = 0;
   scells_E = 0;
   scells_pt = 0;
   scells_eta = 0;
   scells_phi = 0;
   scells_QCells = 0;
   scells_GainCells = 0;
   scells_DetCells = 0;
   scells_TimeCells = 0;
   scells_xCells = 0;
   scells_yCells = 0;
   scells_zCells = 0;
   scells_BadCells = 0;
   scells_IdCells = 0;
   scells_Sigma = 0;
   tt_em_E = 0;
   tt_had_E = 0;
   tt_em_peak = 0;
   tt_had_peak = 0;
   tt_em_adc_peak = 0;
   tt_had_adc_peak = 0;
   tt_em_lut = 0;
   tt_had_lut = 0;
   tt_em_adc = 0;
   tt_had_adc = 0;
   tt_eta = 0;
   tt_phi = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
   fChain->SetBranchAddress("timestamp_ns", &timestamp_ns, &b_timestamp_ns);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("detmask0", &detmask0, &b_detmask0);
   fChain->SetBranchAddress("detmask1", &detmask1, &b_detmask1);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
   fChain->SetBranchAddress("mc_event_number", &mc_event_number, &b_mc_event_number);
   fChain->SetBranchAddress("mc_event_weight", &mc_event_weight, &b_mc_event_weight);
   fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
   fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
   fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
   fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
   fChain->SetBranchAddress("fwdFlags", &fwdFlags, &b_fwdFlags);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("lumiFlags", &lumiFlags, &b_lumiFlags);
   fChain->SetBranchAddress("pixelError", &pixelError, &b_pixelError);
   fChain->SetBranchAddress("sctError", &sctError, &b_sctError);
   fChain->SetBranchAddress("trtError", &trtError, &b_trtError);
   fChain->SetBranchAddress("larError", &larError, &b_larError);
   fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
   fChain->SetBranchAddress("muonError", &muonError, &b_muonError);
   fChain->SetBranchAddress("fwdError", &fwdError, &b_fwdError);
   fChain->SetBranchAddress("coreError", &coreError, &b_coreError);
   fChain->SetBranchAddress("isSimulation", &isSimulation, &b_isSimulation);
   fChain->SetBranchAddress("isCalibration", &isCalibration, &b_isCalibration);
   fChain->SetBranchAddress("isTestBeam", &isTestBeam, &b_isTestBeam);
   fChain->SetBranchAddress("lardigit_sc_n", &lardigit_sc_n, &b_lardigit_sc_n);
   fChain->SetBranchAddress("lardigit_sc_layer", &lardigit_sc_layer, &b_lardigit_sc_layer);
   fChain->SetBranchAddress("lardigit_sc_ieta", &lardigit_sc_ieta, &b_lardigit_sc_ieta);
   fChain->SetBranchAddress("lardigit_sc_calo", &lardigit_sc_calo, &b_lardigit_sc_calo);
   fChain->SetBranchAddress("lardigit_sc_iphi", &lardigit_sc_iphi, &b_lardigit_sc_iphi);
   fChain->SetBranchAddress("lardigit_sc_barrel_ec", &lardigit_sc_barrel_ec, &b_lardigit_sc_barrel_ec);
   fChain->SetBranchAddress("lardigit_sc_pos_neg", &lardigit_sc_pos_neg, &b_lardigit_sc_pos_neg);
   fChain->SetBranchAddress("lardigit_sc_FT", &lardigit_sc_FT, &b_lardigit_sc_FT);
   fChain->SetBranchAddress("lardigit_sc_slot", &lardigit_sc_slot, &b_lardigit_sc_slot);
   fChain->SetBranchAddress("lardigit_sc_channel", &lardigit_sc_channel, &b_lardigit_sc_channel);
   fChain->SetBranchAddress("lardigit_sc_gain", &lardigit_sc_gain, &b_lardigit_sc_gain);
   fChain->SetBranchAddress("lardigit_sc_Samples", &lardigit_sc_Samples, &b_lardigit_sc_Samples);
   fChain->SetBranchAddress("lardigit_sc_offlineId", &lardigit_sc_offlineId, &b_lardigit_sc_offlineId);
   fChain->SetBranchAddress("cc_n", &cc_n, &b_cc_n);
   fChain->SetBranchAddress("cc_E", &cc_E, &b_cc_E);
   fChain->SetBranchAddress("cc_pt", &cc_pt, &b_cc_pt);
   fChain->SetBranchAddress("cc_eta", &cc_eta, &b_cc_eta);
   fChain->SetBranchAddress("cc_phi", &cc_phi, &b_cc_phi);
   fChain->SetBranchAddress("cc_QCells", &cc_QCells, &b_cc_QCells);
   fChain->SetBranchAddress("cc_GainCells", &cc_GainCells, &b_cc_GainCells);
   fChain->SetBranchAddress("cc_DetCells", &cc_DetCells, &b_cc_DetCells);
   fChain->SetBranchAddress("cc_TimeCells", &cc_TimeCells, &b_cc_TimeCells);
   fChain->SetBranchAddress("cc_xCells", &cc_xCells, &b_cc_xCells);
   fChain->SetBranchAddress("cc_yCells", &cc_yCells, &b_cc_yCells);
   fChain->SetBranchAddress("cc_zCells", &cc_zCells, &b_cc_zCells);
   fChain->SetBranchAddress("cc_BadCells", &cc_BadCells, &b_cc_BadCells);
   fChain->SetBranchAddress("cc_IdCells", &cc_IdCells, &b_cc_IdCells);
   fChain->SetBranchAddress("cc_Sigma", &cc_Sigma, &b_cc_Sigma);
   fChain->SetBranchAddress("scells_n", &scells_n, &b_scells_n);
   fChain->SetBranchAddress("scells_E", &scells_E, &b_scells_E);
   fChain->SetBranchAddress("scells_pt", &scells_pt, &b_scells_pt);
   fChain->SetBranchAddress("scells_eta", &scells_eta, &b_scells_eta);
   fChain->SetBranchAddress("scells_phi", &scells_phi, &b_scells_phi);
   fChain->SetBranchAddress("scells_QCells", &scells_QCells, &b_scells_QCells);
   fChain->SetBranchAddress("scells_GainCells", &scells_GainCells, &b_scells_GainCells);
   fChain->SetBranchAddress("scells_DetCells", &scells_DetCells, &b_scells_DetCells);
   fChain->SetBranchAddress("scells_TimeCells", &scells_TimeCells, &b_scells_TimeCells);
   fChain->SetBranchAddress("scells_xCells", &scells_xCells, &b_scells_xCells);
   fChain->SetBranchAddress("scells_yCells", &scells_yCells, &b_scells_yCells);
   fChain->SetBranchAddress("scells_zCells", &scells_zCells, &b_scells_zCells);
   fChain->SetBranchAddress("scells_BadCells", &scells_BadCells, &b_scells_BadCells);
   fChain->SetBranchAddress("scells_IdCells", &scells_IdCells, &b_scells_IdCells);
   fChain->SetBranchAddress("scells_Sigma", &scells_Sigma, &b_scells_Sigma);
   fChain->SetBranchAddress("tt_n", &tt_n, &b_tt_n);
   fChain->SetBranchAddress("tt_em_E", &tt_em_E, &b_tt_em_E);
   fChain->SetBranchAddress("tt_had_E", &tt_had_E, &b_tt_had_E);
   fChain->SetBranchAddress("tt_em_peak", &tt_em_peak, &b_tt_em_peak);
   fChain->SetBranchAddress("tt_had_peak", &tt_had_peak, &b_tt_had_peak);
   fChain->SetBranchAddress("tt_em_adc_peak", &tt_em_adc_peak, &b_tt_em_adc_peak);
   fChain->SetBranchAddress("tt_had_adc_peak", &tt_had_adc_peak, &b_tt_had_adc_peak);
   fChain->SetBranchAddress("tt_em_lut", &tt_em_lut, &b_tt_em_lut);
   fChain->SetBranchAddress("tt_had_lut", &tt_had_lut, &b_tt_had_lut);
   fChain->SetBranchAddress("tt_em_adc", &tt_em_adc, &b_tt_em_adc);
   fChain->SetBranchAddress("tt_had_adc", &tt_had_adc, &b_tt_had_adc);
   fChain->SetBranchAddress("tt_eta", &tt_eta, &b_tt_eta);
   fChain->SetBranchAddress("tt_phi", &tt_phi, &b_tt_phi);
   Notify();
}

Bool_t caloD3PD::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void caloD3PD::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t caloD3PD::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef caloD3PD_cxx
    
