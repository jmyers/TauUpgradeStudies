import code
import ROOT
from ROOT import *
import os
from os import listdir
import glob
from array import array
import sys
from math import sqrt, pow
#print "f"
region = "113_91"
#mk = 22
#clr = EColor(kBlack)
#region = sys.argv[1]
cut = sys.argv[1] 
directory = "/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/"

output = TFile("rateplots/compress200_"+str(cut)+".root","RECREATE")
gridtree = TTree("gridtree","gridtree")
acc =array('f',[0.])
isoe = array('i',[0])
isop = array('i',[0])
coree = array('i',[0])
corep = array('i',[0])
rate = array('f',[0.])
numsd = array('i',[0])

gridtree.Branch('acc',acc,'acc/F')
gridtree.Branch('rate',rate,'rate/F')
gridtree.Branch('isoe',isoe,'isoe/I')
gridtree.Branch('isop',isop,'isop/I')
gridtree.Branch('coree',coree,'coree/I')
gridtree.Branch('corep',corep,'corep/I')
gridtree.Branch('numsd',numsd,'numsd/I')
for filename in os.listdir(directory):
	print filename
	sigfile = TFile(glob.glob(directory+filename+"/*Z*")[0])
	bkgfile = TFile(glob.glob(directory+filename+"/*MB*")[0])
	sigtree = sigfile.Get("mytree")
	bkgtree = bkgfile.Get("mytree")
	events = bkgtree.GetEntries()
	coll = 4.E7
	frac = coll/events
	nbin_fc = 1000;
	nbin_R = 60;
	width = 1./1000.
	h_fcore_M_sig = TH1F("h_fcore_M_sig","",nbin_fc,0,1)
	sigtree.Draw("m_fcore_M>>h_fcore_M_sig","clus_pt_M >"+str(cut)+"000")
	tot = h_fcore_M_sig.Integral(1,nbin_fc+1);
	for i in range(nbin_fc):
		part = h_fcore_M_sig.Integral(1,i+1)
		if(part/tot > 0.05):
			fcr_cut=(i*width)
			print fcr_cut
			break
	h_fcore_bkg = TH1F("h_fcore_bkg","",nbin_fc,0,1)#lower limit was 0.2 I don't remember why so I changed it back to 0
	bkgtree.Draw("m_fcore>>h_fcore_bkg","clus_pt >"+str(cut)+"000")
	count = 0.;
	countfc = 0.;
	events = float(bkgtree.GetEntries());
	for n in range(bkgtree.GetEntries()):
		bkgtree.GetEntry(n)
		nsds = []
		nsdsfc = []
		for m in range(len(bkgtree.clus_pt)):
			if(bkgtree.clus_pt[m] > float(cut)*1000.):
                                nsds.append(bkgtree.clus_pt[m])

			if(bkgtree.clus_pt[m] > float(cut)*1000. and bkgtree.m_fcore[m] > fcr_cut):
				nsdsfc.append(bkgtree.clus_pt[m])
		numsd[0] = len(nsds)
		#print len(nsds)
		if(len(nsds)) > 0:
			count = count + 1;
		if(len(nsdsfc))>0:
			countfc=countfc+1;
	passpct = countfc/count;
	print "check", countfc, count,passpct
	twopct = passpct**2;
	v_rate = twopct*coll;	
	tot_bkg = h_fcore_bkg.Integral(1,nbin_fc+1)
	cutbin = int(fcr_cut/width)
	part_bkg = h_fcore_bkg.Integral(1,cutbin+1)
	left_ovr = h_fcore_bkg.Integral(cutbin+2,nbin_fc+1)
	#v_rate = (left_ovr/tot_bkg**2) * frac
	#v_rate = left_ovr/tot_bkg
	v_accept = 1.- (part_bkg/tot_bkg)
	print part_bkg, tot_bkg
	rate[0]= (v_rate)
	acc[0] = (passpct)
	print v_rate, v_accept
	sigtree.GetEntry(0)
	v_isoe = sigtree.isoeta
	v_isop = sigtree.isophi
	v_coree = sigtree.coreeta
	v_corep = sigtree.corephi
	print v_isoe, v_isop, v_coree, v_corep
	isoe[0] = (v_isoe)
	isop[0] = (v_isop)
	coree[0] = (v_coree)
	corep[0] = (v_corep)
	output.cd()
	gridtree.Fill()
output.cd()
output.Write()
output.Close()

#print rate, rej
##################################################################

#code.interact(local=locals())
