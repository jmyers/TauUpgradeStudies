import code
import ROOT
from ROOT import *
import os
import sys
from array import array
from math import sqrt, pow
region = sys.argv[1]
#cut = sys.argv[2]
sigfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_"+str(region)+"/output_Z200_"+str(region)+".root")
sigtree = sigfile.Get("mytree")
bkgfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_"+str(region)+"/output_MB200_"+str(region)+".root")
bkgtree = bkgfile.Get("mytree")
output = TFile("rateplots/ROCred__"+str(region)+".root","RECREATE")
out = TTree("mytree","mytree")
effout = array('f',[0.])
rateout = array('f',[0.])
out.Branch('effout'+str(region),effout,'/F')
out.Branch('rateout'+str(region),rateout,'/F')

def params1D(temp):

    #temp.SetTitleOffset(1.1,"X")
    #temp.SetTitleOffset(1.7,"Y")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    #xaxis.CenterTitle()
    xaxis.SetTitleSize(0.03)
    xaxis.SetLabelSize(0.03)
    xaxis.SetTitleOffset(1.2)
    #yaxis.CenterTitle()
    yaxis.SetTitleSize(0.03)
    yaxis.SetLabelSize(0.03)
    yaxis.SetTitleOffset(1.7)
    return None
gStyle.SetOptStat(0)
nbin_fc = 1000;
width = 1./1000.
nbins = 400
h_rate=TH1F("h_rate","Single Tau L1 Trigger Rate",40,10,50)
h_ratefc95=TH1F("h_ratefc95","",40,10,50)


h_pass=TH1F("h_pass","",200,0,200)
ROC = TGraph()
pointer = TGraph()
fcr_cut = -999.;
minthresh = 0.;
stepsize = 1;
nsteps = 50;
coll = 30766320
events = float(bkgtree.GetEntries());

countfc95 = 0.
bcidomit = [62,63,64,65,66,67,68,69,70,71,72,73,768,770,771,772,773,774,775,776,777,778,779,780,1302,1303,1304,1305,1306,1307,1308,1309,1310,1311,1312,1313,1663,1664,1665,1666,1667,1668,1669,1670,1671,1672,1673,1674,2196,2197,2198,2199,2200,2201,2202,2203,2204,2205,2206,2207,2731,2732,2733,2734,2735,2736,2737,2738,2739,2740,2741,2742]
events_adj = 0
events_tau_adj = 0
for q in range(nsteps):
	limit = float(q*1000)
	print "limit", limit
	h_fcore_M_siglp = TH1F("h_fcore_M_siglp","",nbin_fc,0,1)
	for entry in range(sigtree.GetEntries()):
		sigtree.GetEntry(entry)
		#print "signal", sigtree.bcId
		'''if sigtree.bcId not in bcidomit:
			#print "here"
			continue;
		if q == 0:
			events_tau_adj = events_tau_adj + 1
		'''
		for seeds in range(len(sigtree.m_fcore_M)):
			pt = sigtree.clus_pt_M[seeds]
			iso = sigtree.m_iso_pt_M[seeds]- sigtree.m_core_pt_M[seeds]
                        nring = ((sigtree.isoeta * sigtree.isophi)-(sigtree.coreeta * sigtree.corephi))
                        nl2 = sigtree.nlayer2_M[seeds]
                        iso_av = iso / nring
                        reduction = iso_av * nl2
			pt_red = pt -reduction
			if pt_red > limit:
				h_fcore_M_siglp.Fill(sigtree.m_fcore_M[seeds])

	#sigtree.Draw("m_fcore_M>>h_fcore_M_siglp","clus_pt_M>"+str(limit)+""))#+" && m_decay==2")
	totlp = h_fcore_M_siglp.Integral(1,nbin_fc+1);
	fcr_cutlp = -999.
	fcr_cutlp95 = -999.
	there = 0;
	for i in range(nbin_fc):
		partlp = h_fcore_M_siglp.Integral(1,i+1)
		if(partlp/totlp>0.05 and there ==0):
			fcr_cutlp95=(i*width)
			there = 1
			#print "here"
		if(partlp/totlp > 0.1):
			fcr_cutlp=(i*width)
			break

	h_passfc95=TH1F("h_passfc95","",200,0,200)
	for m in range(bkgtree.GetEntries()):
		bkgtree.GetEntry(m);
		#print "bkg", bkgtree.bcId
		'''if bkgtree.bcId not in bcidomit:
			#print "here"
			continue;
		if q == 0:
			events_adj = events_adj + 1;
		'''
		nsdsfc95 = []
		tcsfc = []
		for z in range(len(bkgtree.clus_pt)):
			pt = bkgtree.clus_pt[z]#/1000.
			iso = bkgtree.m_iso_pt[z]- bkgtree.m_core_pt[z]
                        nring = ((bkgtree.isoeta * bkgtree.isophi)-(bkgtree.coreeta * bkgtree.corephi))
                        nl2 = bkgtree.nlayer2[z]
                        iso_av = iso / nring
                        reduction = iso_av * nl2
                        pt_red = (pt -reduction)/1000.
			tcsfc.append(pt_red)
			if(bkgtree.m_fcore[z] > fcr_cutlp95):
				nsdsfc95.append(pt_red)
		nsdsfc95.sort()
		nsdsfc95.reverse()
	
		if(len(nsdsfc95)) > 0:
                        countfc95 = countfc95 + 1;
                if(len(nsdsfc95)==0):
                        continue;
                h_passfc95.Fill(nsdsfc95[0])
	
	
	leftfc95 = h_passfc95.Integral(stepsize*q+2,nbins+1)
	ratefc95 = (leftfc95/events)*coll
	#ratefc952 = ((leftfc95/events)**2)*coll
	ROC.SetPoint(q,((totlp*0.95)/(2*events)),ratefc95)
	if q == 22:
		#print "here"
		pointer.SetPoint(0,((totlp*0.95)/(2*events)),ratefc95)
	h_fcore_M_siglp.Delete()	
	h_passfc95.Delete()
	effout[0] = ((totlp*0.95)/(2*events))
	rateout[0] = ratefc95
	print q,effout[0],rateout[0]
	out.Fill()
output.cd()
##################################################################

ROC.GetXaxis().SetTitle("Signal Acceptance")
ROC.GetYaxis().SetTitle("Background Rate [Hz]")
ROC.SetLineColor(kRed)
ROC.SetMarkerColor(kRed)
pointer.SetMarkerStyle(34)
pointer.SetMarkerSize(2)
pointer.SetMarkerColor(kBlack)
c4 = TCanvas("c4","",800,600)
c4.cd()
c4.SetLogy(1)
ROC.Draw()
pointer.Draw("PSAME")
c4.Print("ROCred.png")
output.Write()
output.Close()
sys.stdout.flush()
#code.interact(local=locals())
