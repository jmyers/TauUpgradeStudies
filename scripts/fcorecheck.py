import code
import ROOT
from ROOT import *
import os
import sys

def params1D(temp):

    #temp.SetTitleOffset(1.1,"X")
    #temp.SetTitleOffset(1.7,"Y")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    #xaxis.CenterTitle()
    xaxis.SetTitleSize(0.03)
    xaxis.SetLabelSize(0.03)
    xaxis.SetTitleOffset(1.2)
    #yaxis.CenterTitle()
    yaxis.SetTitleSize(0.03)
    yaxis.SetLabelSize(0.03)
    yaxis.SetTitleOffset(1.7)
    return None


region = sys.argv[1]
sigfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_"+str(region)+"/output_Z200_"+str(region)+".root")
sigtree = sigfile.Get("mytree")
bkgfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_"+str(region)+"/output_MB200_"+str(region)+".root")
bkgtree = bkgfile.Get("mytree")
nbin_fc = 100
width = 1./100.
h_sig = TH1F("h_sig","F_{Core}",nbin_fc,0,1)
h_bkg = TH1F("h_bkg","F_{Core}",nbin_fc,0,1)
h_fcrvspt = TH2F("h_fcrvpt","",50,0,100,100,-10,10)
sigtree.Draw("m_fcore_M>>h_sig","clus_pt_M>1000")
bkgtree.Draw("m_fcore>>h_bkg","clus_pt>1000")
bkgtree.Draw("m_fcore:clus_pt>>h_fcrvpt")
fcr_cut = -999.
sigtot = h_sig.Integral(1,nbin_fc+1)
bkgtot = h_bkg.Integral(1,nbin_fc+1)
for i in range(nbin_fc):
	part = h_sig.Integral(1,i+1)
	if(part/sigtot > 0.05):
		fcr_cut=(i*width)
		break
val = TLine(fcr_cut,0,fcr_cut,0.038)
val.SetLineWidth(1)
gStyle.SetOptStat(0)
	
print h_bkg.Integral(int(fcr_cut/width)+1,nbin_fc+1), h_bkg.Integral(1,nbin_fc+1)
h_sig.Scale(1./sigtot)
h_bkg.Scale(1./bkgtot)

h_fcrvspt.SetLineColor(kBlack)
h_sig.SetLineColor(kRed)
h_bkg.SetLineColor(kBlue)
h_bkg.SetXTitle("F_{Core}")
h_bkg.SetYTitle("#frac{1}{N} #frac{dN}{dX}")
params1D(h_sig)
leg = TLegend(0.7,0.7,0.9,0.9)
leg.AddEntry(h_sig,"h->#tau #tau","l")
leg.AddEntry(h_bkg,"MinBias <#mu>=200","l")
leg.AddEntry(h_fcrvpt,"95% Signal Efficiency","l")
c1 = TCanvas("c1","",800,600)
c1.cd()
h_bkg.Draw()
h_sig.Draw("SAME")
val.Draw("SAME")
leg.Draw()
c1.Print("fcr200_"+str(region)+"_10.png")
c2 = TCanvas("c2","",800,600)
c2.cd()
h_fcrvspt.Draw("COLZ")
c2.Print("sdffsd.png")
#code.interact(local=locals())
