import ROOT
import code
from ROOT import *
import os
from math import sqrt
sigfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_95_32/output_Z200_95_32.root")
sigtree = sigfile.Get("mytree")

bkgfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_95_32/output_MB200_95_32.root")
bkgtree = bkgfile.Get("mytree")
#Find fcrcut
width = 1./1000.
nbin_fc = 1000
fcr_cut = -999.
h_fcore_M_sig = TH1F("h_fcore_M_sig","",nbin_fc,0,1)
sigtree.Draw("m_fcore_M>>h_fcore_M_sig","clus_pt_M >22000")
tot = h_fcore_M_sig.Integral(1,nbin_fc+1);
for i in range(nbin_fc):
        part = h_fcore_M_sig.Integral(1,i+1)
        if(part/tot > 0.05):
                fcr_cut=(i*width)
                break
#do other shit
roc = TH1D("roc","",100,0,100)
eve = TH2D("eve","",100,0,100,100,0,100)
eve2 = TH2D("eve2","",100,0,100,100,0,100)
t22bkg = TProfile("t22bkg","",50,0,100)
t22 = TProfile("t22","",50,0,100)
t30 = TProfile("t30","",50,0,100)
t40 = TProfile("t40","",50,0,100)
bkgpass = 0
for n in range(sigtree.GetEntries()):
	trupts = []
	sdpts = []
	sdfcr = []
	bkgsrt = []
	bkgfcr = []
	spt_red = []
	sigtree.GetEntry(n)
	bkgtree.GetEntry(n)
	for f in range(len(bkgtree.clus_pt)):
		bkgsrt.append(bkgtree.clus_pt[f])
		bkgfcr.append(bkgtree.m_fcore[f])
	bkgsrt, bkgfcr = zip(*sorted(zip(bkgsrt,bkgfcr)))
	bkgpt = list(bkgsrt)
	bkgfc = list(bkgfcr)
	bkgpt.reverse() 
	bkgfc.reverse()
	
	for m in range(len(sigtree.m_true_pt)):
		teta = sigtree.m_true_eta[m]
		tphi = sigtree.m_true_phi[m]
		tpt = sigtree.m_true_pt[m]
		decay = sigtree.m_decay[m]
		#if decay != 2:
		#	continue;
		for o in range(len(sigtree.m_seed_eta_M)):
			seta = sigtree.m_seed_eta_M[o]
	                sphi = sigtree.m_seed_phi_M[o]
        	        spt = sigtree.clus_pt_M[o]
			iso = sigtree.m_iso_pt_M[o]- sigtree.m_core_pt_M[o]
			nring = ((sigtree.isoeta * sigtree.isophi)-(sigtree.coreeta * sigtree.corephi))
			nl2 = sigtree.nlayer2_M[o]
			iso_av = iso / nring
			#print "pt",iso,"n",nring, "iso Average", iso_av,nl2
			reduction = iso_av * nl2
			#print spt,reduction, (spt-reduction)	
			# Need to add individual layer energy outputs to C++ file so I can 
			# subtrace the iso_av from the included layer 2 scells	
			# I suspect I will also need to write out the layer2 nscells included
			# because while it should be constant, it might not always be
			
			dR = sqrt(pow((seta-teta),2) + pow((sphi-tphi),2))
			if (dR < 0.2):
				trupts.append(tpt)
				sdpts.append(spt)
				spt_red.append(spt-reduction)
				sdfcr.append(sigtree.m_fcore_M[o])
				break
		if(len(sdpts) != len(trupts)):
			sdpts.append(-999.)
			sdfcr.append(-999.)
	
	for q in range(len(trupts)):
		if sdpts[q] > 22000 and sdfcr[q] > fcr_cut:
			accept22 = 1
		else:
			accept22 = 0
		eve.Fill(trupts[q]/1000.,sdpts[q]/1000.)
		eve2.Fill(trupts[q]/1000.,spt_red[q]/1000.)

		t22.Fill(trupts[q]/1000.,accept22)
		if sdpts[q] > 30000 and sdfcr[q] > fcr_cut:
			accept30 = 1
		else:
			accept30 = 0
		t30.Fill(trupts[q]/1000.,accept30)
		if sdpts[q] > 40000 and sdfcr[q] > fcr_cut:
			accept40 = 1
		else:
			accept40 = 0
		t40.Fill(trupts[q]/1000.,accept40)

t22.SetMarkerStyle(25)
t22.SetMarkerColor(kRed)
t30.SetMarkerStyle(26)
t30.SetMarkerColor(kGreen-6)
t40.SetMarkerStyle(28)
t40.SetMarkerColor(kBlue)
gStyle.SetOptStat(0)
t22.SetXTitle("True Visible Tau Energy [GeV]")
t22.SetYTitle("Acceptance")
leg = TLegend(0.1,0.7,0.4,0.9)
leg.AddEntry(t22,"TC > 22GeV, 95% F_{Core}","p")
#leg.AddEntry(t30,"TC > 30GeV, 95% F_{Core}","p")
#leg.AddEntry(t40,"TC > 40GeV, 95% F_{Core}","p")
c1 = TCanvas("c1","",800,600)
t22.Draw()
#t30.Draw("SAME")
#t40.Draw("SAME")
#leg.Draw()
c1.Print("Turnon.png")
c2 = TCanvas("c2","",800,600)
c2.cd()
eve.Draw("COLZ")
c2.Print("eve2.png")
c3 = TCanvas("c3","",800,600)
t22bkg.Draw()
c3.Print("shit.png")
c4 = TCanvas("c4","",800,600)
c4.cd()
eve2.Draw("COLZ")
#c4.Print("eve2.png")
#code.interact(local=locals())

