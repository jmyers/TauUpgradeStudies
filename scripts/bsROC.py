from ROOT import *
import code
import os
f0 = TFile("rateplots/ROC_95_32.root")
t0 = f0.Get("mytree")
f1 = TFile("rateplots/ROC500__95_32.root")
t1 = f1.Get("mytree")
f2 = TFile("rateplots/ROC1000__95_32.root")
t2 = f2.Get("mytree")
f3 = TFile("rateplots/ROC750__95_32.root")
t3 = f3.Get("mytree")
f4 = TFile("rateplots/ROC250__95_32.root")
t4 = f4.Get("mytree")

ROC = TGraph()
ROC500 = TGraph()
ROC1000 = TGraph()
ROC750 = TGraph()
ROC250 = TGraph()

for q in range(t0.GetEntries()):
	print q	
	t0.GetEntry(q)
	t1.GetEntry(q)
	t2.GetEntry(q)
	t3.GetEntry(q)
	t4.GetEntry(q)
	ROC.SetPoint(q,t0.effout95_32,t0.rateout95_32)
	ROC500.SetPoint(q,t1.effout95_32,t1.rateout95_32)
	ROC1000.SetPoint(q,t2.effout95_32,t2.rateout95_32)
	ROC750.SetPoint(q,t3.effout95_32,t3.rateout95_32)
	ROC250.SetPoint(q,t4.effout95_32,t4.rateout95_32)
	

ROC.GetXaxis().SetTitle("Truth Acceptance")
ROC.GetYaxis().SetTitle("Background Rate")
ROC.SetLineColor(kBlack)
ROC500.GetXaxis().SetTitle("Truth Acceptance")
ROC500.GetYaxis().SetTitle("Background Rate")
ROC500.SetLineColor(kRed)

ROC1000.GetXaxis().SetTitle("Truth Acceptance")
ROC1000.GetYaxis().SetTitle("Background Rate")
ROC1000.SetLineColor(kGreen-6)

ROC750.SetLineColor(kViolet+4)
ROC250.SetLineColor(kOrange-6)

leg = TLegend(0.7,0.1,0.9,0.3)
leg.AddEntry(ROC,"No Thresh","l")
leg.AddEntry(ROC1000,"1000MeV Thresh","l")
leg.AddEntry(ROC750,"750MeV Thresh","l")
leg.AddEntry(ROC500,"500MeV Thresh","l")
leg.AddEntry(ROC250,"250MeV Thresh","l")

c1 = TCanvas("c1","",800,600)
c1.cd()
c1.SetLogy(1)
ROC.Draw()
ROC500.Draw("SAME")
ROC1000.Draw("SAME")
ROC750.Draw("SAME")
ROC250.Draw("SAME")

leg.Draw()
c1.Print("ROC_thresh.png")
#code.interact(local=locals())
                                
