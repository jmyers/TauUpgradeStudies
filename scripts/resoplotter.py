import code
import ROOT
from ROOT import *
import os
import sys

region = "113_91"
mk = 22
clr = EColor(kBlack)
sigfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output/output_Z200_"+str(region)+".root")
sigtree = sigfile.Get("mytree")
bkgfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output/output_MB200_"+str(region)+".root")
bkgtree = bkgfile.Get("mytree")

output = TFile("rateplots/output_"+str(region)+".root","RECREATE")
def params1D(temp):

    #temp.SetTitleOffset(1.1,"X")
    #temp.SetTitleOffset(1.7,"Y")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    #xaxis.CenterTitle()
    xaxis.SetTitleSize(0.03)
    xaxis.SetLabelSize(0.03)
    xaxis.SetTitleOffset(1.2)
    #yaxis.CenterTitle()
    yaxis.SetTitleSize(0.03)
    yaxis.SetLabelSize(0.03)
    yaxis.SetTitleOffset(1.7)
    return None
gStyle.SetOptStat(0)
nbin_fc = 100;
nbin_R = 60;
width = 1./100.
h_rate=TH1F("h_rate","",20,0,10E4)
h_res=TH1F("h_res","",200,-10,10)
h_rej=TGraph()
fcr_cut = [];
minthresh = 0.;
stepsize = 5000.;
nsteps = 20;
for n in range(nsteps):
	h_fcore_M_sig = TH1F("h_fcore_M_sig"+str(n),"",nbin_fc,0,1)
	sigtree.Draw("m_fcore_M>>h_fcore_M_sig"+str(n),"clus_pt_M >"+str(minthresh+stepsize*n))
	tot = h_fcore_M_sig.Integral(1,nbin_fc+1);
	for i in range(nbin_fc):
		part = h_fcore_M_sig.Integral(1,i+1)
		if(part/tot > 0.05):
			fcr_cut.append(i*width)
			break

for n in range(nsteps):
        h_fcore_bkg = TH1F("h_fcore_bkg"+str(n),"",nbin_fc,0.2,1)
	bkgtree.Draw("m_fcore>>h_fcore_bkg"+str(n),"clus_pt >"+str(minthresh+stepsize*n))		
	tot_bkg = h_fcore_bkg.Integral(1,nbin_fc+1)
	if n == 0:
		ucut = tot_bkg
	cutbin = int(fcr_cut[n]/width)
	part_bkg = h_fcore_bkg.Integral(1,cutbin+1)
	left_ovr = h_fcore_bkg.Integral(cutbin+1,nbin_fc+1)
	rate = 40.*left_ovr/ucut
	h_rate.Fill(n*stepsize,rate)
	rej = part_bkg/tot_bkg
	h_rej.SetPoint(n,n*stepsize,rej)

for n in range(sigtree.GetEntries()):
	print n
	sigtree.GetEntry(n)
	if(len(sigtree.clus_pt_M) != len(sigtree.m_true_pt)):
		continue
	for i in range(len(sigtree.clus_pt_M)):
		print sigtree.clus_pt_M[i], sigtree.m_true_pt[i]
sigtree.Draw("((clus_pt_M[0]-m_true_pt[0])/m_true_pt[0])>>h_res")
sigtree.Draw("((clus_pt_M[1]-m_true_pt[1])/m_true_pt[1])>>+h_res")
##################################################################
h_res.SetLineColor(clr)
params1D(h_res)
h_res.SetXTitle("#frac{TauCluster Energy- True Visible Energy}{True Visible Energy}")
h_res.SetYTitle("Events")
#c3 = TCanvas("c3","",800,600)
#c3.cd()
#h_res.Draw()
h_res.Write()

################################################################
params1D(h_rate)
h_rate.SetXTitle("Tau Cluster [MeV]")
h_rate.SetYTitle("Rate [MHz]")
h_rate.SetLineColor(clr)
#c8 = TCanvas("c8","",800,600)
#c8.cd()
#c8.SetLogy(1)
#h_rate.Draw("HIST")
h_rate.Write()
########################################################
leg = TLegend(0.40,0.2,0.60,0.35);
leg.AddEntry(h_rej,"f_{core} #frac{(7x1)}{(11x3)}","p");
params1D(h_rej)
#######################################################
h_rej.GetXaxis().SetTitle("Tau Cluster Thresh [MeV]")
h_rej.GetYaxis().SetTitle("Background Rejection")
h_rej.SetMarkerStyle(mk)
h_rej.SetMarkerColor(clr)
#h_rej.SetLineColor(kBlack)
#c2 = TCanvas("c2","",800,600)
#c2.cd()
#h_rej.Draw("ap")
#leg.Draw()
h_rej.Write()
#c2.Print("rej.root")
#c8.Print("Rate.root")
code.interact(local=locals())
