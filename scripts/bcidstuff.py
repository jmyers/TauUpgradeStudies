import code
import ROOT
from ROOT import *
import os
import sys
region = sys.argv[1]
bkgfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_"+str(region)+"/output_MB200_"+str(region)+".root")
bkgtree = bkgfile.Get("mytree")
def params1D(temp):

    #temp.SetTitleOffset(1.1,"X")
    #temp.SetTitleOffset(1.7,"Y")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    #xaxis.CenterTitle()
    xaxis.SetTitleSize(0.03)
    xaxis.SetLabelSize(0.03)
    xaxis.SetTitleOffset(1.2)
    #yaxis.CenterTitle()
    yaxis.SetTitleSize(0.03)
    yaxis.SetLabelSize(0.03)
    yaxis.SetTitleOffset(1.7)
    return None
gStyle.SetOptStat(0)
h_pass=TH1F("h_pass","",3500,0,3500)
h_pass1=TH1F("h_pass1","",200,-2.5,2.5)
for n in range(bkgtree.GetEntries()):
	bkgtree.GetEntry(n)
	h_pass.Fill(bkgtree.bcId,len(bkgtree.clus_pt))
	for roi in range(len(bkgtree.clus_pt)):
		h_pass1.Fill(bkgtree.m_seed_eta[roi])
		
		
for n in range(3500):
	print n, h_pass.GetBinContent(n)
	if(h_pass.GetBinContent(n)>500):
		print "here"

h_pass.SetXTitle("BCID")
h_pass.SetYTitle("Number of ROIs")
params1D(h_pass)

h_pass1.SetXTitle("#eta")
h_pass1.SetYTitle("Number of ROIs")
params1D(h_pass1)

c1 = TCanvas("c1","",800,600)
c1.cd()
h_pass.Draw()
c1.Print("roivsbcid.png")

c2 = TCanvas("c2","",800,600)
c2.cd()
h_pass1.Draw()
c2.Print("roivseta.png")

code.interact(local=locals())
