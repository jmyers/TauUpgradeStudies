import code
import ROOT
from ROOT import *
import os
from math import fabs
import sys
region = sys.argv[1]
sigfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_"+str(region)+"/output_Z200_"+str(region)+".root")
sigtree = sigfile.Get("mytree")
def params1D(temp):

    #temp.SetTitleOffset(1.1,"X")
    #temp.SetTitleOffset(1.7,"Y")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    #xaxis.CenterTitle()
    xaxis.SetTitleSize(0.03)
    xaxis.SetLabelSize(0.03)
    xaxis.SetTitleOffset(1.2)
    #yaxis.CenterTitle()
    yaxis.SetTitleSize(0.03)
    yaxis.SetLabelSize(0.03)
    yaxis.SetTitleOffset(1.7)
    return None
gStyle.SetOptStat(0)
h_dphi=TH1F("h_dphi","",60,0,6)
h_deta=TH1F("h_deta","",60,0,6)
for n in range(sigtree.GetEntries()):
	sigtree.GetEntry(n)
	sigsrt = []
	sigdum = []
	sigdume = []
	for f in range(len(sigtree.m_true_pt)):
		sigsrt.append(sigtree.m_true_pt[f])
        	sigdum.append(sigtree.m_true_phi[f])
        	sigdume.append(sigtree.m_true_eta[f])
        if len(sigsrt) < 2:
		continue
	sigsrt, sigdum = zip(*sorted(zip(sigsrt,sigdum)))
	sigsrt, sigdume = zip(*sorted(zip(sigsrt,sigdume)))
	
        trupt = list(sigsrt)
        truphi = list(sigdum)
	trueta = list(sigdume)
	trupt.reverse()
	trueta.reverse()
	truphi.reverse()
	m_dphi = ROOT.TVector2.Phi_mpi_pi(truphi[0]-truphi[1])
	m_deta = trueta[0]-trueta[1]
	h_dphi.Fill(fabs(m_dphi))
	h_deta.Fill(fabs(m_deta))
h_dphi.SetXTitle("#delta#phi")
h_dphi.SetYTitle("Entries")
h_deta.SetXTitle("#delta#eta")
h_deta.SetYTitle("Entries")
params1D(h_deta)
params1D(h_dphi)

c1 = TCanvas("c1","",800,600)
c1.cd()
h_dphi.Draw()
c1.Print("true_dphi.png")

c2 = TCanvas("c2","",800,600)
c2.cd()
h_deta.Draw()
c2.Print("true_deta.png")
#code.interact(local=locals())
