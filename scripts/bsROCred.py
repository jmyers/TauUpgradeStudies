from ROOT import *
import code
import os
f0 = TFile("rateplots/ROC_95_32.root")
t0 = f0.Get("mytree")
f1 = TFile("rateplots/ROCred__95_32.root")
t1 = f1.Get("mytree")

ROC = TGraph()
ROCred = TGraph()
for q in range(t0.GetEntries()):
	print q	
	t0.GetEntry(q)
	t1.GetEntry(q)
	#t2.GetEntry(q)
	ROC.SetPoint(q,t0.effout95_32,t0.rateout95_32)
	ROCred.SetPoint(q,t1.effout95_32,t1.rateout95_32)
	#ROCredbcid.SetPoint(q,t2.effout95_32,t2.rateout95_32)
	

ROC.GetXaxis().SetTitle("Truth Acceptance")
ROC.GetYaxis().SetTitle("Background Rate")
ROC.SetLineColor(kBlack)
ROCred.GetXaxis().SetTitle("Truth Acceptance")
ROCred.GetYaxis().SetTitle("Background Rate")
ROCred.SetLineColor(kRed)
#ROCredbcid.GetXaxis().SetTitle("Truth Acceptance")
#ROCredbcid.GetYaxis().SetTitle("Background Rate")
#ROCredbcid.SetLineColor(kGreen)


leg = TLegend(0.7,0.1,0.9,0.3)
leg.AddEntry(ROC,"No Reduction","l")
leg.AddEntry(ROCred,"Iso Reduced","l")
#leg.AddEntry(ROCredbcid,"Iso/Bcid Red","l")

c1 = TCanvas("c1","",800,600)
c1.cd()
c1.SetLogy(1)
ROC.Draw()
ROCred.Draw("SAME")
#ROCredbcid.Draw("SAME")
leg.Draw()
c1.Print("ROC_red_comp.png")
#code.interact(local=locals())
                                
