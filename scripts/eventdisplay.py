import code
import ROOT
from ROOT import *
import os
from math import pi, sin, atan, exp,sqrt
ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
ROOT.xAOD.Init() # Initialize the xAOD infrastructure
AODtreeName = "CollectionTree"
myAODChain = ROOT.TChain(AODtreeName)
myAODChain.Add("/scratch/jmyers10/Z0/user.aagaard.mc15_13TeV.341124.ggH125_tautauhh_r7081.v5_EXT0/user.aagaard.6548391.EXT0._000012.NTUP.root")
t = ROOT.xAOD.MakeTransientTree( myAODChain ) #Make the "transient tree"


def params2D(temp):
    temp.SetTitleOffset(1.0,"Y")
    temp.SetTitleOffset(0.5,"Z")
    temp.SetTitleOffset(1,"X")
    #temp->SetMinimum(-100);
#    temp.SetMaximum(350);
    temp.SetYTitle("#phi")
    temp.SetXTitle("#eta")
    temp.SetZTitle("E_{T}")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    zaxis = temp.GetZaxis()
    xaxis.CenterTitle()
    xaxis.SetTitleSize(0.025)
    xaxis.SetLabelSize(0.025)
    yaxis.CenterTitle()
    yaxis.SetTitleSize(0.025)
    yaxis.SetLabelSize(0.025)
    zaxis.CenterTitle()
    zaxis.SetTitleSize(0.025)
    zaxis.SetLabelSize(0.025)
    #temp.SetMinimum(0)
    return None


def Layer(detCells):
        calo = -999;
        layer =  -999;
        isEM = True;
        #--------------------------------------------------------------------------------
        if (detCells == 81 or detCells == 273 or detCells ==145):#front
                layer = 1;
                calo = 0;
        #--------------------------------------------------------------------------------
        elif (detCells == 97 or detCells == 289 or detCells ==161):#middle
                layer = 2;
                calo = 0;
        #--------------------------------------------------------------------------------
        elif (detCells == 65 or detCells == 257):#PreS
                layer = 0;
                calo = 0;
        #--------------------------------------------------------------------------------
        elif (detCells == 113 or detCells == 305):#Back
                layer = 3;
                calo = 0;
        #--------------------------------------------------------------------------------
        elif (detCells == 65544 or detCells == 131080):#Tile 0
                layer = 0;
                calo = 2;
                isEM = False;
        #--------------------------------------------------------------------------------
        elif (detCells == 2):#HEC
                layer = 0;
                calo = 1;
                isEM = False;
        #--------------------------------------------------------------------------------
        elif (detCells == 2052):#FCAL 0
                layer = 0;
                calo = 3;
        #--------------------------------------------------------------------------------
        elif (detCells == 4100 or detCells == 131080):#FCAL 1
                layer = 1;
                calo = 3;
                isEM = False;
        #--------------------------------------------------------------------------------
        elif (detCells == 6148):#FCAL 2
                layer = 2;
                calo = 3;
                isEM = False;
        #--------------------------------------------------------------------------------
        elif (detCells == 81928 or detCells == 147464):#Tile Layer D
                layer = 1;
                calo = 2;
                isEM = False;
        #--------------------------------------------------------------------------------
        return layer, calo, isEM;



myFile = TFile("/scratch/jmyers10/Z0/user.aagaard.mc15_13TeV.341124.ggH125_tautauhh_r7081.v5_EXT0/user.aagaard.6548391.EXT0._000012.NTUP.root")
datatree = myFile.Get("caloD3PD")
output = TFile("event_disp.root","RECREATE")
c1 = TCanvas("c1", "", 600, 600);
c2 = TCanvas("c2","",600,600);
c3 = TCanvas("c3","",600,600);
c4 = TCanvas("c4","",600,600);
c5 = TCanvas("c5","",600,600);
c6 = TCanvas("c6","",600,600);
#gStyle.SetOptStat(0);


hist_em = TH2F("hist_em","", 140,-7, 7, 100, -5,5)
hist_had = TH2F("hist_had","", 140,-7, 7, 100, -5, 5)
hist_tt = TH2F("hist_tt","", 140,-7, 7, 100, -5, 5)
hist_scell0 = TH2F("hist_scell0","",140,-7,7,100, -5, 5)
hist_scell1 = TH2F("hist_scell1","",560,-7,7,100, -5, 5)
hist_scell2 = TH2F("hist_scell2","",560,-7,7,100, -5, 5)
hist_scell3 = TH2F("hist_scell3","",140,-7,7,100, -5, 5)
h_true = TH2F("h_true","",1400,-7,7,1000,-5,5)

params2D(hist_em)
params2D(hist_had)
params2D(hist_scell0)
params2D(hist_scell3)
params2D(hist_scell2)
params2D(hist_scell1)
E_em = 0.0
E_had = 0.0
event = 10; 
datatree.GetEntry(event)
t.GetEntry(event);
pdgIds=[]
children=[]
parents=[]
pts=[]
etas=[]
phis=[]
ms=[]
charges=[]
barcodes=[]
nChildren=[]
Vis_taus = []
n=0
TLV_Vis = TLorentzVector(0,0,0,0)
TLV_True = TLorentzVector(0,0,0,0)
TLV_Vis_tmp = TLorentzVector(0,0,0,0)
TLV_tt = TLorentzVector(0,0,0,0)
TLV_sc = TLorentzVector(0,0,0,0)
TLV_sc2 = TLorentzVector(0,0,0,0)
EM_Towers = TLorentzVector(0,0,0,0)
Had_Towers = TLorentzVector(0,0,0,0)
Tower_List = []
Sc_List = []
Sc_List2 = []
tau_tag = []
delR_ttem_0 = 0.
delR_ttem_1 = 0.
for particle in t.TruthParticles:
	num_pi = 0;
	num_pi0 = 0;
	lepton_tag = 0;
	if abs(particle.pdgId())==15 and particle.status()==2:
		#print "parent", particle.parent().pdgId()
		#print particle.pdgId()
		Vis_taus.append(TLorentzVector(0,0,0,0))
		len(Vis_taus)
		TLV_True.SetPtEtaPhiM(particle.pt(),particle.eta(),particle.phi(),particle.m())
		for cl in range(particle.nChildren()):
			TLV_Vis_tmp.SetPtEtaPhiM(0,0,0,0)
			childPdgId=particle.child(cl).pdgId()

			if abs(childPdgId) == 211:
				num_pi = num_pi + 1;
			if abs(childPdgId) == 111:
				num_pi0 = num_pi0 + 1;

			print "child", childPdgId
			if abs(childPdgId) == 11 or abs(childPdgId)==13:
				lepton_tag = 1
			if abs(childPdgId) != 12 and abs(childPdgId) != 14 and abs(childPdgId) != 16:
				TLV_Vis_tmp.SetPtEtaPhiM(particle.child(cl).pt(),particle.child(cl).eta(),particle.child(cl).phi(),particle.child(cl).m());
				#TLV_Vis = TLV_Vis + TLV_Vis_tmp
				Vis_taus[n] = Vis_taus[n]+TLV_Vis_tmp
			#print "(",particle.child(cl).pt(),", ",particle.child(cl).eta(),", ",particle.child(cl).phi(),")"
		if lepton_tag == 1:
			print "Leptonic Tau"
			continue
		delR_T_V = TLV_True.DeltaR(Vis_taus[n])

		if num_pi == 1 and num_pi0 == 0:
			#1 charged pion 0 neutral pions
			tau_tag.append(0);
		elif num_pi == 1 and num_pi0 > 0:
			#1 charged pion 1+ neutral pions
			tau_tag.append(1);
		elif num_pi == 3:
			#3 charged pions 0+ neutral pions
			tau_tag.append(2);
		else:
			# Other
			tau_tag.append(-999);
		n = 1
for q in range(len(Vis_taus)):

	h_true.Fill(Vis_taus[q].Eta(),Vis_taus[q].Phi())

for obj in range(datatree.tt_n):
	eta = datatree.tt_eta[obj]
	phi = TVector2.Phi_mpi_pi(datatree.tt_phi[obj])
	E = datatree.tt_had_E[obj] + datatree.tt_em_E[obj]
	hist_tt.Fill(eta,phi,E)
for obj in range(datatree.scells_n):
		eta = datatree.scells_eta[obj]
		phi = datatree.scells_phi[obj]
		layer = Layer(datatree.scells_DetCells[obj])[0]
		E = datatree.scells_pt[obj]/1000.
		
		if E > 0:
			if layer == 0:
				hist_scell0.Fill(eta,phi,E) 
			if layer == 1:
				hist_scell1.Fill(eta,phi,E)
			if layer == 2:
				hist_scell2.Fill(eta,phi,E)
			if layer == 3:
				hist_scell3.Fill(eta,phi,E)

hist_scell2.SetXTitle("Supercell #eta")
hist_scell2.SetYTitle("Supercell #phi")
hist_scell2.SetZTitle("Supercell pt [GeV]")
h_true.SetMarkerStyle(3)
h_true.SetMarkerSize(1)
hist_tt.Write()
hist_scell0.Write()
hist_scell1.Write()
hist_scell2.Write()
hist_scell3.Write()
h_true.Write()
code.interact(local=locals())
