
#! /usr/bin/env python

"""

The plotting can be done on each file on it's own and output the histograms to a saved file
This code splits the config file into single jobs per dsid and outputs the histograms
It uses the 'secret' plotting flag (NOPLOTS) in order to NOT output any plots

"""

import os, optparse, sys

parser = optparse.OptionParser()
parser.add_option("-f", dest="folder",      help="Folder conatining inputs",                          default = 0)
parser.add_option("-c", dest="configFile",  help="Config File for plotting",                          default = 0)
(options, args) = parser.parse_args()

total_jobs = 0
#rm """ + new_config + """
#rm """ + script_name + """* 

def SendJob( dsid, syst, new_config ):
    pwd = os.getcwd()
    script_name = "BatchPlot_" + syst + "_" + dsid + ".sh"
    script = open( script_name, 'w' )
    script.write( """#! /bin/bash
    cd """ + pwd + """;
    source VBF_root.sh
    cd """ + pwd + """;
    ./plot """ + options.folder + """ """ + new_config + """
    """)
    script.close()
    os.system( "chmod a+x " + script_name )

    condor_name = "condor_" + syst + "_" + dsid + ".condor"
    condor = open( condor_name, 'w' )
    condor.write( """Executable   = """ + script_name + """
    Universe = vanilla
    Environment = "HOME=$ENV(HOME) PATH=$ENV(PATH)"
    Output = save_""" + script_name + """_o
    Error = save_"""  + script_name + """_e
    Log = save_"""  + script_name + """_l
    Notification = Error
    Queue   1
    """)
    condor.close()
    os.system( "condor_submit " + condor_name )
    pass
    
if __name__ == "__main__":
    
    if options.folder == 0 or options.configFile == 0:
        print "Please enter a folder (-f) and a config file (-c), exiting"
        sys.exit(1)
        pass
    
    config_file = open( options.configFile )
    config_lines = config_file.readlines()

    systematics = []
    systematics.append("NOMINAL")
    for line in config_lines:
        line_sections = line.split(" ")
        if not "%" in line and "syst:" in line_sections[0]:
            for i in xrange(1, len(line_sections) ):
                systematics.append( line_sections[i].rstrip("\n") )
                pass
            pass
        pass
    
    for syst in systematics:
        # Check each line for a dsid keyword
        print "Submitting jobs for systematic: " + syst
        for line in config_lines:
            if "%" not in line and "dataset:" in line:
                line_sections = line.split(" ")
                for i in xrange( 2,  len(line_sections) -1 ):
                    #if not line_sections[i] == "129917": continue
                    if "period" in line and not syst == "NOMINAL": continue
                    if "Multijets" in line and not syst == "NOMINAL": continue
                    new_line = line_sections[0] + " " + line_sections[1] + " " + line_sections[i] + " " + line_sections[-1]
                    out_file_name = options.configFile + line_sections[i] + "_" + syst
                    out_file = open( out_file_name, 'w' )
                    for sub_line in config_lines:
                        if sub_line == line: out_file.write( new_line )
                        elif "%" not in sub_line and "dataset:" in sub_line: continue
                        elif "%" not in sub_line and "syst:" in sub_line: continue
                        else:                out_file.write( sub_line )
                        pass
                    if not syst == "NOMINAL":
                        out_file.write("NONOM \n")
                        out_file.write("NOMULT \n")
                        out_file.write("syst: " + syst + " \n")
                        pass
                    out_file.write( "NOPLOT" )
                    out_file.close()
                    SendJob( line_sections[i], syst, out_file_name )
                    total_jobs += 1
                    pass
                pass
            pass
        pass
    print "Total jobs submitted: " + str(total_jobs)
    pass

