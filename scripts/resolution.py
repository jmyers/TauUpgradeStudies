import code
import ROOT
from ROOT import *
import os
import sys
from math import sqrt, pow
sigfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/output_debug_12_2.root")
sigtree = sigfile.Get("mytree")
#bkgfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output/output_113_71/output_MB200_113_71.root")
#bkgtree = bkgfile.Get("mytree")
output = TFile("resplots.root","RECREATE")
def params1D(temp):
    #temp.SetTitleOffset(1.1,"X")
    #temp.SetTitleOffset(1.7,"Y")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    #xaxis.CenterTitle()
    xaxis.SetTitleSize(0.03)
    xaxis.SetLabelSize(0.03)
    xaxis.SetTitleOffset(1.2)
    #yaxis.CenterTitle()
    yaxis.SetTitleSize(0.03)
    yaxis.SetLabelSize(0.03)
    yaxis.SetTitleOffset(1.7)
    return None
gStyle.SetOptStat(0)
h_res=TH1F("h_res","",400,-2,2)
h_res_2=TH2F("h_res_2","",20,0,200,400,-2,2)
h_e_2=TH2F("h_e_2","",20,0,200,100,0,200)
h_mindr=TH1F("h_mindr","",100,0,1)
for n in range(sigtree.GetEntries()):
	sigtree.GetEntry(n)
	#print n
	for sd in range(len(sigtree.m_seed_eta)):
		mindr = 999.
		for true in range(len(sigtree.m_true_pt)):
			dPhi = TVector2.Phi_mpi_pi(sigtree.m_seed_phi[sd]-sigtree.m_true_phi[true])
			dEta = sigtree.m_seed_eta[sd] - sigtree.m_true_eta[true]
			dR = sqrt(pow(dEta,2)+pow(dPhi,2))
			if dR < mindr:
				mindr = dR
			#print mindr
			if dR < 0.2 and sigtree.clus_pt_M[sd] > 0.:
				#print sigtree.clus_pt_M[sd], sigtree.m_true_pt[true]
				res = (sigtree.clus_pt_M[sd] - sigtree.m_true_pt[true])/sigtree.m_true_pt[true];
				h_res.Fill(res)
				pt = sigtree.m_true_pt[true]/1000.
				clus = sigtree.clus_pt_M[sd]/1000.
				#print sigtree.m_true_pt[true],res
				h_res_2.Fill(pt,res)
				h_e_2.Fill(pt,clus)
				break;
		h_mindr.Fill(mindr)


##################################################################
params1D(h_res)
h_res.SetXTitle("#frac{TauCluster Energy- True Visible Energy}{True Visible Energy}")
h_res.SetYTitle("Events")
h_res.Write()

params1D(h_res_2)
h_res_2.SetXTitle("True Visible Energy")
h_res_2.SetYTitle("#frac{TauCluster Energy- True Visible Energy}{True Visible Energy}")
h_res_2.Write()

params1D(h_e_2)
h_e_2.SetXTitle("True Visible Energy[MeV]")
h_e_2.SetYTitle("Clus Energy[MeV]")
h_e_2.Write()

params1D(h_mindr)
h_mindr.SetYTitle("Events")
h_mindr.SetXTitle("Minimum #delta R(truth,custer)")
h_mindr.Write()
################################################################
#code.interact(local=locals())
