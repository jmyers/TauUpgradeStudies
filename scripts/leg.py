import ROOT
from ROOT import *
import code


a=TH1F("a","",1,0,1)
a.SetMarkerStyle(20)
a.SetMarkerColor(kRed)

b=TH1F("b","",1,0,1)
b.SetMarkerStyle(21)
b.SetMarkerColor(kOrange-3)

c=TH1F("c","",1,0,1)
c.SetMarkerStyle(22)
c.SetMarkerColor(kYellow-3)

d=TH1F("d","",1,0,1)
d.SetMarkerStyle(23)
d.SetMarkerColor(kSpring-7)

e=TH1F("e","",1,0,1)
e.SetMarkerStyle(24)
e.SetMarkerColor(kGreen+1)

f=TH1F("f","",1,0,1)
f.SetMarkerStyle(25)
f.SetMarkerColor(kTeal-7)

g=TH1F("g","",1,0,1)
g.SetMarkerStyle(26)
g.SetMarkerColor(kCyan+1)


h=TH1F("h","",1,0,1)
h.SetMarkerStyle(27)
h.SetMarkerColor(kAzure-3)

i=TH1F("i","",1,0,1)
i.SetMarkerStyle(28)
i.SetMarkerColor(kBlue+1)

j=TH1F("j","",1,0,1)
j.SetMarkerStyle(29)
j.SetMarkerColor(kBlack)

c2=TCanvas("c2","",600,800)
c2.cd()
leg=TLegend(0.1,0.1,0.9,0.9)

leg.AddEntry(a,"115_91","p")
leg.AddEntry(b,"115_71","p")
leg.AddEntry(c,"115_51","p")
leg.AddEntry(d,"115_31","p")
leg.AddEntry(e,"115_11","p")
leg.AddEntry(f,"113_91","p")
leg.AddEntry(g,"113_71","p")
leg.AddEntry(h,"113_51","p")
leg.AddEntry(i,"113_31","p")
leg.AddEntry(j,"113_11","p")
a.Draw()
leg.Draw()
c2.Print("leg.png")

code.interact(local=locals())

