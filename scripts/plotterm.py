import code
import ROOT
from ROOT import *
import os
import sys
from math import sqrt, pow
region = sys.argv[1]
cut = sys.argv[2]
sigfile73_31 = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_73_31/output_Z200_73_31.root")
sigtree73_31 = sigfile73_31.Get("mytree")
bkgfile73_31 = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_73_31/output_MB200_73_31.root")
bkgtree73_31 = bkgfile73_31.Get("mytree")
output = TFile("rateplots/output200.root","RECREATE")
def params1D(temp):

    #temp.SetTitleOffset(1.1,"X")
    #temp.SetTitleOffset(1.7,"Y")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    #xaxis.CenterTitle()
    xaxis.SetTitleSize(0.03)
    xaxis.SetLabelSize(0.03)
    xaxis.SetTitleOffset(1.2)
    #yaxis.CenterTitle()
    yaxis.SetTitleSize(0.03)
    yaxis.SetLabelSize(0.03)
    yaxis.SetTitleOffset(1.7)
    return None
gStyle.SetOptStat(0)
nbin_fc = 1000;
width = 1./1000.
nbins = 400
h_rate73_31=TH1F("h_rate73_31","",40,10,50)

h_rate73_312=TH1F("h_rate73_312","",40,10,50)

h_rate73_31fc95=TH1F("h_rate73_31fc95","",40,10,50)

h_rate73_31fc952=TH1F("h_rate73_31fc952","",40,10,50)




h_TCh=TH1F("h_TCh","",nbins,-10,200)
h_TChfc=TH1F("h_TChfc","",nbins,-10,200)
h_e_2=TH2F("h_e_2","",20,0,200,100,0,200)

h_mindr=TH1F("h_mindr","",100,0,1)
h_rej=TH1F("h_rej","",40,10,50)
h_pass=TH1F("h_pass","",200,0,200)

fcr_cut = -999.;
minthresh = 0.;
stepsize = 1;
nsteps = 50;
coll = 30766320
events = float(bkgtree73_31.GetEntries());
for m in range(bkgtree73_31.GetEntries()):
	bkgtree73_31.GetEntry(m);
	nsds = []
	tcs = []
	for r in range(len(bkgtree73_31.clus_pt)):
		pt = bkgtree73_31.clus_pt[r]/1000.
		tcs.append(pt)
		if(pt > float(cut)):
			nsds.append(pt)
	tcs.sort()
	tcs.reverse()
	h_pass.Fill(tcs[0])

for q in range(nsteps):
	left = h_pass.Integral(stepsize*q+2,nbins+1)
	rate = (left/events)*coll
	rate2 = ((left/events)**2)*coll
	h_rate73_31.Fill(stepsize*q,rate)
	h_rate73_312.Fill(stepsize*q,rate2)
	limit = float(q*1000)
	h_fcore_M_siglp = TH1F("h_fcore_M_siglp","",nbin_fc,0,1)
	sigtree73_31.Draw("m_fcore_M>>h_fcore_M_siglp","clus_pt_M>"+str(limit))
	totlp = h_fcore_M_siglp.Integral(1,nbin_fc+1);
	fcr_cutlp = -999.
	fcr_cutlp95 = -999.
	for i in range(nbin_fc):
		partlp = h_fcore_M_siglp.Integral(1,i+1)
		if(partlp/totlp>0.05):
			fcr_cutlp95=(i*width)
			break

	h_passfc95=TH1F("h_passfc95","",200,0,200)
	for m in range(bkgtree73_31.GetEntries()):
		bkgtree73_31.GetEntry(m);
		nsdsfc95 = []
		for z in range(len(bkgtree73_31.clus_pt)):
			pt = bkgtree73_31.clus_pt[z]/1000.
			if(bkgtree73_31.m_fcore[z] > fcr_cutlp95):
				nsdsfc95.append(pt)
		nsdsfc95.sort()
		nsdsfc95.reverse()
		h_passfc.Fill(nsdsfc[0])
                h_passfc95.Fill(nsdsfc95[0])

	leftfc95 = h_passfc95.Integral(stepsize*q+2,nbins+1)
	ratefc95 = (leftfc95/events)*coll
	ratefc952 = ((leftfc95/events)**2)*coll
	if(q>9):	
		h_rate73_31fc95.Fill(stepsize*q,ratefc95)
		h_rate73_31fc952.Fill(stepsize*q,ratefc952)
	
	h_fcore_M_siglp.Delete()	
	h_passfc.Delete()
	h_passfc95.Delete()
	if(leftfc95==0):
		continue
	h_rej.Fill(stepsize*q,leftfc95/left)
##################################################################
params1D(h_rej)
h_rej.SetMarkerSize(1)
h_rej.SetMarkerStyle(29)
h_rej.SetYTitle("F_{core} Acceptance")
h_rej.SetXTitle("TC Threshold [GeV]")
h_rej.Write()

leg = TLegend(0.5,0.7,0.9,0.9)

c2 = TCanvas("c2","",800,600)
c2.cd()
c2.SetLogy(0)
h_rej.Draw("PHISTO")
c2.Print("acceptance_multi_200.eps")
#code.interact(local=locals())
