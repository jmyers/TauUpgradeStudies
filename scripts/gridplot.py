import ROOT 
from ROOT import *
import code
import sys

cut = sys.argv[1]

def params2D(temp):
    temp.SetTitleOffset(1.0,"Y")
    temp.SetTitleOffset(1.0,"Z")
    temp.SetTitleOffset(1,"X")
    #temp->SetMinimum(-100);
#    temp.SetMaximum(350);
    temp.SetYTitle("#phi")
    temp.SetXTitle("#eta")
    temp.SetZTitle("E_{T}")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    zaxis = temp.GetZaxis()
    #xaxis.CenterTitle()
    xaxis.SetTitleSize(0.025)
    xaxis.SetLabelSize(0.025)
    yaxis.CenterTitle()
    yaxis.SetTitleSize(0.025)
    yaxis.SetLabelSize(0.025)
    zaxis.CenterTitle()
    zaxis.SetTitleSize(0.025)
    zaxis.SetLabelSize(0.025)
    #temp.SetMinimum(0)
    return None

f = TFile("~/Tau_analysis/EventLoopTutorial/rateplots/compress200_"+str(cut)+".root")
t = f.Get("gridtree")

h_11_rate = TH2F("h_11_rate","",6,0,12,9,1,10)
h_9_rate = TH2F("h_9_rate","",6,0,12,9,1,10)
h_7_rate = TH2F("h_7_rate","",6,0,12,9,1,10)
h_5_rate = TH2F("h_5_rate","",6,0,12,9,1,10)
h_3_rate = TH2F("h_3_rate","",6,0,12,9,1,10)

h_11_acc = TH2F("h_11_acc","",6,0,12,9,1,10)
h_9_acc = TH2F("h_9_acc","",6,0,12,9,1,10)
h_7_acc = TH2F("h_7_acc","",6,0,12,9,1,10)
h_5_acc = TH2F("h_5_acc","",6,0,12,9,1,10)
h_3_acc = TH2F("h_3_acc","",6,0,12,9,1,10)

h_seeds = TH1F("h_seeds","",11,0,10)

gStyle.SetOptStat(0)
for entry in range(t.GetEntries()):
	t.GetEntry(entry)
	ie = t.isoe
	ip = t.isop
	ce = t.coree
	cp = t.corep
	acc = t.acc
	rate = t.rate
	seeds = t.numsd
	h_seeds.Fill(seeds)
	if ie == 11:
		if ip == 3 and cp == 1:
			h_11_rate.Fill(ce,1,rate)
			h_11_acc.Fill(ce,1,acc)
		if ip == 3 and cp == 2:
			h_11_rate.Fill(ce,2,rate)
			h_11_acc.Fill(ce,2,acc)
		if ip == 3 and cp == 3:
			h_11_rate.Fill(ce,3,rate)
			h_11_acc.Fill(ce,3,acc)
                if ip == 4 and cp == 1:
                        h_11_rate.Fill(ce,4,rate)
                        h_11_acc.Fill(ce,4,acc)
                if ip == 4 and cp == 2:
                        h_11_rate.Fill(ce,5,rate)
                        h_11_acc.Fill(ce,5,acc)
                if ip == 4 and cp == 3:
                        h_11_rate.Fill(ce,6,rate)
                        h_11_acc.Fill(ce,6,acc)
		if ip == 5 and cp == 1:
			h_11_rate.Fill(ce,7,rate)
			h_11_acc.Fill(ce,7,acc)
		if ip == 5 and cp == 2:
                        h_11_rate.Fill(ce,8,rate)
                        h_11_acc.Fill(ce,8,acc)
		if ip == 5 and cp == 3:
			h_11_rate.Fill(ce,9,rate)
			h_11_acc.Fill(ce,9,acc)

        if ie == 9:
		if ip == 3 and cp == 1:
                        h_9_rate.Fill(ce,1,rate)
                        h_9_acc.Fill(ce,1,acc)
                if ip == 3 and cp == 2:
                        h_9_rate.Fill(ce,2,rate)
                        h_9_acc.Fill(ce,2,acc)
                if ip == 3 and cp == 3:
                        h_9_rate.Fill(ce,3,rate)
                        h_9_acc.Fill(ce,3,acc)
                if ip == 4 and cp == 1:
                        h_9_rate.Fill(ce,4,rate)
                        h_9_acc.Fill(ce,4,acc)
                if ip == 4 and cp == 2:
                        h_9_rate.Fill(ce,5,rate)
                        h_9_acc.Fill(ce,5,acc)
                if ip == 4 and cp == 3:
                        h_9_rate.Fill(ce,6,rate)
                        h_9_acc.Fill(ce,6,acc)
                if ip == 5 and cp == 1:
                        h_9_rate.Fill(ce,7,rate)
                        h_9_acc.Fill(ce,7,acc)
                if ip == 5 and cp == 2:
                        h_9_rate.Fill(ce,8,rate)
                        h_9_acc.Fill(ce,8,acc)
                if ip == 5 and cp == 3:
                        h_9_rate.Fill(ce,9,rate)
                        h_9_acc.Fill(ce,9,acc)
 

        if ie == 7:
		if ip == 3 and cp == 1:
                        h_7_rate.Fill(ce,1,rate)
                        h_7_acc.Fill(ce,1,acc)
                if ip == 3 and cp == 2:
                        h_7_rate.Fill(ce,2,rate)
                        h_7_acc.Fill(ce,2,acc)
                if ip == 3 and cp == 3:
                        h_7_rate.Fill(ce,3,rate)
                        h_7_acc.Fill(ce,3,acc)
                if ip == 4 and cp == 1:
                        h_7_rate.Fill(ce,4,rate)
                        h_7_acc.Fill(ce,4,acc)
                if ip == 4 and cp == 2:
                        h_7_rate.Fill(ce,5,rate)
                        h_7_acc.Fill(ce,5,acc)
                if ip == 4 and cp == 3:
                        h_7_rate.Fill(ce,6,rate)
                        h_7_acc.Fill(ce,6,acc)
                if ip == 5 and cp == 1:
                        h_7_rate.Fill(ce,7,rate)
                        h_7_acc.Fill(ce,7,acc)
                if ip == 5 and cp == 2:
                        h_7_rate.Fill(ce,8,rate)
                        h_7_acc.Fill(ce,8,acc)
                if ip == 5 and cp == 3:
                        h_7_rate.Fill(ce,9,rate)
                        h_7_acc.Fill(ce,9,acc)
 
        if ie == 5:
                if ip == 3 and cp == 1:
                        h_5_rate.Fill(ce,1,rate)
                        h_5_acc.Fill(ce,1,acc)
                if ip == 3 and cp == 2:
                        h_5_rate.Fill(ce,2,rate)
                        h_5_acc.Fill(ce,2,acc)
                if ip == 3 and cp == 3:
                        h_5_rate.Fill(ce,3,rate)
                        h_5_acc.Fill(ce,3,acc)
                if ip == 4 and cp == 1:
                        h_5_rate.Fill(ce,4,rate)
                        h_5_acc.Fill(ce,4,acc)
                if ip == 4 and cp == 2:
                        h_5_rate.Fill(ce,5,rate)
                        h_5_acc.Fill(ce,5,acc)
                if ip == 4 and cp == 3:
                        h_5_rate.Fill(ce,6,rate)
                        h_5_acc.Fill(ce,6,acc)
                if ip == 5 and cp == 1:
                        h_5_rate.Fill(ce,7,rate)
                        h_5_acc.Fill(ce,7,acc)
                if ip == 5 and cp == 2:
                        h_5_rate.Fill(ce,8,rate)
                        h_5_acc.Fill(ce,8,acc)
                if ip == 5 and cp == 3:
                        h_5_rate.Fill(ce,9,rate)
                        h_5_acc.Fill(ce,9,acc)




        if ie == 3:
		if ip == 3 and cp == 1:
                        h_3_rate.Fill(ce,1,rate)
                        h_3_acc.Fill(ce,1,acc)
                if ip == 3 and cp == 2:
                        h_3_rate.Fill(ce,2,rate)
                        h_3_acc.Fill(ce,2,acc)
                if ip == 3 and cp == 3:
                        h_3_rate.Fill(ce,3,rate)
                        h_3_acc.Fill(ce,3,acc)
                if ip == 4 and cp == 1:
                        h_3_rate.Fill(ce,4,rate)
                        h_3_acc.Fill(ce,4,acc)
                if ip == 4 and cp == 2:
                        h_3_rate.Fill(ce,5,rate)
                        h_3_acc.Fill(ce,5,acc)
                if ip == 4 and cp == 3:
                        h_3_rate.Fill(ce,6,rate)
                        h_3_acc.Fill(ce,6,acc)
                if ip == 5 and cp == 1:
                        h_3_rate.Fill(ce,7,rate)
                        h_3_acc.Fill(ce,7,acc)
                if ip == 5 and cp == 2:
                        h_3_rate.Fill(ce,8,rate)
                        h_3_acc.Fill(ce,8,acc)
                if ip == 5 and cp == 3:
                        h_3_rate.Fill(ce,9,rate)
                        h_3_acc.Fill(ce,9,acc)
 



 

params2D(h_11_rate)
h_11_rate.SetXTitle("Core #eta")
h_11_rate.SetYTitle("#phi = 3,1 | #phi = 3,2 | #phi = 3,3 | #phi = 4,1 | #phi = 4,2 | #phi = 4,3 | #phi = 5,1 | #phi = 5,2 | #phi = 5,3")
h_11_rate.SetZTitle("Rate [Hz]")

params2D(h_9_rate)
h_9_rate.SetXTitle("Core #eta")
h_9_rate.SetYTitle("#phi = 3,1 | #phi = 3,2 | #phi = 3,3 | #phi = 4,1 | #phi = 4,2 | #phi = 4,3 | #phi = 5,1 | #phi = 5,2 | #phi = 5,3")
h_9_rate.SetZTitle("Rate [Hz]")

params2D(h_7_rate)
h_7_rate.SetXTitle("Core #eta")
h_7_rate.SetYTitle("#phi = 3,1 | #phi = 3,2 | #phi = 3,3 | #phi = 4,1 | #phi = 4,2 | #phi = 4,3 | #phi = 5,1 | #phi = 5,2 | #phi = 5,3")
h_7_rate.SetZTitle("Rate [Hz]")

params2D(h_5_rate)
h_5_rate.SetXTitle("Core #eta")
h_5_rate.SetYTitle("#phi = 3,1 | #phi = 3,2 | #phi = 3,3 | #phi = 4,1 | #phi = 4,2 | #phi = 4,3 | #phi = 5,1 | #phi = 5,2 | #phi = 5,3")
h_5_rate.SetZTitle("Rate [Hz]")

params2D(h_3_rate)
h_3_rate.SetXTitle("Core #eta")
h_3_rate.SetYTitle("#phi = 3,1 | #phi = 3,2 | #phi = 3,3 | #phi = 4,1 | #phi = 4,2 | #phi = 4,3 | #phi = 5,1 | #phi = 5,2 | #phi = 5,3")
h_3_rate.SetZTitle("Rate [Hz]")


params2D(h_11_acc)
h_11_acc.SetXTitle("Core #eta")
h_11_acc.SetYTitle("#phi = 3,1 | #phi = 3,2 | #phi = 3,3 | #phi = 4,1 | #phi = 4,2 | #phi = 4,3 | #phi = 5,1 | #phi = 5,2 | #phi = 5,3")
h_11_acc.SetZTitle("Acceptance")

params2D(h_9_acc)
h_9_acc.SetXTitle("Core #eta")
h_9_acc.SetYTitle("#phi = 3,1 | #phi = 3,2 | #phi = 3,3 | #phi = 4,1 | #phi = 4,2 | #phi = 4,3 | #phi = 5,1 | #phi = 5,2 | #phi = 5,3")
h_9_acc.SetZTitle("Acceptance")

params2D(h_7_acc)
h_7_acc.SetXTitle("Core #eta")
h_7_acc.SetYTitle("#phi = 3,1 | #phi = 3,2 | #phi = 3,3 | #phi = 4,1 | #phi = 4,2 | #phi = 4,3 | #phi = 5,1 | #phi = 5,2 | #phi = 5,3")
h_7_acc.SetZTitle("Acceptance")

params2D(h_5_acc)
h_5_acc.SetXTitle("Core #eta")
h_5_acc.SetYTitle("#phi = 3,1 | #phi = 3,2 | #phi = 3,3 | #phi = 4,1 | #phi = 4,2 | #phi = 4,3 | #phi = 5,1 | #phi = 5,2 | #phi = 5,3")
h_5_acc.SetZTitle("Acceptance")

params2D(h_3_acc)
h_3_acc.SetXTitle("Core #eta")
h_3_acc.SetYTitle("#phi = 3,1 | #phi = 3,2 | #phi = 3,3 | #phi = 4,1 | #phi = 4,2 | #phi = 4,3 | #phi = 5,1 | #phi = 5,2 | #phi = 5,3")
h_3_acc.SetZTitle("Acceptance")

h_seeds.SetXTitle("number of seeds")

c1 = TCanvas("c1","",800,600)
c1.cd()
h_11_rate.Draw("COLZTEXT")
c1.Print("TC"+str(cut)+"/rate11.png")
c2 = TCanvas("c2","",800,600)
c2.cd()
h_11_acc.Draw("COLZTEXT")
c2.Print("TC"+str(cut)+"/acc11.png")

c3 = TCanvas("c3","",800,600)
c3.cd()
h_9_rate.Draw("COLZTEXT")
c3.Print("TC"+str(cut)+"/rate9.png")
c4 = TCanvas("c4","",800,600)
c4.cd()
h_9_acc.Draw("COLZTEXT")
c4.Print("TC"+str(cut)+"/acc9.png")

c5 = TCanvas("c5","",800,600)
c5.cd()
h_7_rate.Draw("COLZTEXT")
c5.Print("TC"+str(cut)+"/rate7.png")
c6 = TCanvas("c6","",800,600)
c6.cd()
h_7_acc.Draw("COLZTEXT")
c6.Print("TC"+str(cut)+"/acc7.png")

c7 = TCanvas("c7","",800,600)
c7.cd()
h_5_rate.Draw("COLZTEXT")
c7.Print("TC"+str(cut)+"/rate5.png")
c8 = TCanvas("c8","",800,600)
c8.cd()
h_5_acc.Draw("COLZTEXT")
c8.Print("TC"+str(cut)+"/acc5.png")

c9 = TCanvas("c9","",800,600)
c9.cd()
h_3_rate.Draw("COLZTEXT")
c9.Print("TC"+str(cut)+"/rate3.png")
c10 = TCanvas("c10","",800,600)
c10.cd()
h_3_acc.Draw("COLZTEXT")
c10.Print("TC"+str(cut)+"/acc3.png")

c11 = TCanvas("c11","",800,600)
c11.cd()
h_seeds.Draw()
c11.Print("TC"+str(cut)+"/nseeds.png")
code.interact(local=locals())
