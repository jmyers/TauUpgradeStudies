import code
import ROOT
from ROOT import *
import os
import sys
from array import array
from math import sqrt, pow
region = sys.argv[1]
#cut = sys.argv[2]
sigfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_"+str(region)+"/output_Z200_"+str(region)+".root")
sigtree = sigfile.Get("mytree")
bkgfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_"+str(region)+"/output_MB200_"+str(region)+".root")
bkgtree = bkgfile.Get("mytree")

def params1D(temp):

    #temp.SetTitleOffset(1.1,"X")
    #temp.SetTitleOffset(1.7,"Y")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    #xaxis.CenterTitle()
    xaxis.SetTitleSize(0.03)
    xaxis.SetLabelSize(0.03)
    xaxis.SetTitleOffset(1.2)
    #yaxis.CenterTitle()
    yaxis.SetTitleSize(0.03)
    yaxis.SetLabelSize(0.03)
    yaxis.SetTitleOffset(1.7)
    return None
gStyle.SetOptStat(0)
nbin_fc = 1000;
width = 1./1000.
nbins = 400
h_sig = TH1F("h_sig","",100,0,100)
h_sig_red = TH1F("h_sig_red","",100,0,100)

h_bkg = TH1F("h_bkg","",100,0,100)
h_bkg_red = TH1F("h_bkg_red","",100,0,100)

for event in range(sigtree.GetEntries()):
	sigtree.GetEntry(event)
	for m in range(len(sigtree.m_true_pt)):
		teta = sigtree.m_true_eta[m]
		tphi = sigtree.m_true_phi[m]
		tpt = sigtree.m_true_pt[m]
		decay = sigtree.m_decay[m]
		#if decay != 2:
		#       continue;
		for o in range(len(sigtree.m_seed_eta_M)):
			seta = sigtree.m_seed_eta_M[o]
			sphi = sigtree.m_seed_phi_M[o]
			spt = sigtree.clus_pt_M[o]
			iso = sigtree.m_iso_pt_M[o]- sigtree.m_core_pt_M[o]
			nring = ((sigtree.isoeta * sigtree.isophi)-(sigtree.coreeta * sigtree.corephi))
			nl2 = sigtree.nlayer2_M[o]
			iso_av = iso / nring
			reduction = iso_av * nl2

			dR = sqrt(pow((seta-teta),2) + pow((sphi-tphi),2))
			if (dR < 0.2):
				h_sig.Fill(spt/1000.)
				h_sig_red.Fill((spt-reduction)/1000.)
				#break

	
for event in range(bkgtree.GetEntries()):
	bkgtree.GetEntry(event)
	for o in range(len(bkgtree.clus_pt)):
		spt = bkgtree.clus_pt[o]
		iso = bkgtree.m_iso_pt[o]- bkgtree.m_core_pt[o]
		nring = ((bkgtree.isoeta * bkgtree.isophi)-(bkgtree.coreeta * bkgtree.corephi))
		nl2 = bkgtree.nlayer2[o]
		iso_av = iso / nring
		reduction = iso_av * nl2

		h_bkg.Fill(spt/1000.)
		h_bkg_red.Fill((spt-reduction)/1000.)
			#break

h_sig.SetLineColor(kBlack)
h_sig_red.SetLineColor(kRed)
h_sig.SetXTitle("Pt [GeV]")
h_sig.SetYTitle("Events")
h_bkg.SetLineColor(kBlack)
h_bkg_red.SetLineColor(kRed)
h_bkg_red.SetXTitle("Pt [GeV]")
h_bkg_red.SetYTitle("Events")



leg = TLegend(0.6,0.7,0.9,0.9)
leg.AddEntry(h_sig,"No Reduction","l")
leg.AddEntry(h_sig_red,"BCID Reduction","l")
c1 = TCanvas("c1","",800,600)
c1.cd()
h_sig.Draw("")
h_sig_red.Draw("SAME")
leg.Draw()
c1.Print("sigpt.png")
c2 = TCanvas("c2","",800,600)
c2.cd()
h_sig_red.Draw("COLZ")
c2.Print("sigpt_red.png")
c3 = TCanvas("c3","",800,600)
c3.cd()
h_bkg_red.Draw("")
h_bkg.Draw("SAME")
leg.Draw()
c3.Print("bkgpt.png")
c4 = TCanvas("c4","",800,600)
c4.cd()
h_bkg_red.Draw("COLZ")
c4.Print("bkgpt_red.png")
#code.interact(local=locals())
