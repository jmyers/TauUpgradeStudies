import code
import ROOT
from ROOT import *
import os
import sys
from array import array
from math import sqrt, pow
#print "f"
#region = "113_91"
region = sys.argv[1]
#mk = 22
#mk = int(sys.argv[2])
#clr = EColor(kBlack)
#clr = EColor(eval(sys.argv[3]))
#print region, mk, clr
cut = sys.argv[2]
sigfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_"+str(region)+"/output_Z200_"+str(region)+".root")
sigtree = sigfile.Get("mytree")
bkgfile = TFile("/export/home/jmyers10/Tau_analysis/EventLoopTutorial/Output200/output_"+str(region)+"/output_MB200_"+str(region)+".root")
bkgtree = bkgfile.Get("mytree")
output = TFile("rateplots/output200_red"+str(region)+".root","RECREATE")
out = TTree("mytree","mytree")
def params1D(temp):

    #temp.SetTitleOffset(1.1,"X")
    #temp.SetTitleOffset(1.7,"Y")
    xaxis = temp.GetXaxis()
    yaxis = temp.GetYaxis()
    #xaxis.CenterTitle()
    xaxis.SetTitleSize(0.03)
    xaxis.SetLabelSize(0.03)
    xaxis.SetTitleOffset(1.2)
    #yaxis.CenterTitle()
    yaxis.SetTitleSize(0.03)
    yaxis.SetLabelSize(0.03)
    yaxis.SetTitleOffset(1.7)
    return None
gStyle.SetOptStat(0)
nbin_fc = 1000;
width = 1./1000.
nbins = 400
h_rate=TH1F("h_rate","Single Tau L1 Trigger Rate",40,10,50)

h_rate2=TH1F("h_rate2","Di-Tau L1 Trigger Rate",40,10,50)

h_ratefc90=TH1F("h_ratefc90","",40,10,50)

h_ratefc902=TH1F("h_ratefc902","",40,10,50)

h_ratefc95=TH1F("h_ratefc95","",40,10,50)

h_ratefc952=TH1F("h_ratefc952","",40,10,50)



h_TCmb=TH1F("h_TCmb","",nbins,-10,200)
h_TCmbfc=TH1F("h_TCmbfc","",nbins,-10,200)

h_TCh=TH1F("h_TCh","",nbins,-10,200)
h_TChfc=TH1F("h_TChfc","",nbins,-10,200)
h_e_2=TH2F("h_e_2","",20,0,200,100,0,200)

h_mindr=TH1F("h_mindr","",100,0,1)
h_rej=TH1F("h_rej"+str(region),"",40,10,50)
h_pass=TH1F("h_pass","",200,0,200)

cutout = array('f',[0.])
rejout = array('f',[0.])
out.Branch('cutout'+str(region),cutout,'/F')
out.Branch('rejout'+str(region),rejout,'/F')
fcr_cut = -999.;
minthresh = 0.;
stepsize = 1;
nsteps = 50;
coll = 30766320
events = float(bkgtree.GetEntries());
bcidomit = [62,63,64,65,66,67,68,69,70,71,72,73,768,770,771,772,773,774,775,776,777,778,779,780,1302,1303,1304,1305,1306,1307,1308,1309,1310,1311,1312,1313,1663,1664,1665,1666,1667,1668,1669,1670,1671,1672,1673,1674,2196,2197,2198,2199,2200,2201,2202,2203,2204,2205,2206,2207,2731,2732,2733,2734,2735,2736,2737,2738,2739,2740,2741,2742]

for n in range(sigtree.GetEntries()):
        sigtree.GetEntry(n);
	tcs = []
	for p in range(len(sigtree.clus_pt_M)):
        	pt = sigtree.clus_pt_M[p]/1000.
        	h_TCh.Fill(pt)

h_fcore_M_sig = TH1F("h_fcore_M_sig","",nbin_fc,0,1)
sigtree.Draw("m_fcore_M>>h_fcore_M_sig","clus_pt_M >"+str(cut)+"000")
tot = h_fcore_M_sig.Integral(1,nbin_fc+1);
for i in range(nbin_fc):
	part = h_fcore_M_sig.Integral(1,i+1)
	if(part/tot > 0.05):
		fcr_cut=(i*width)
		#print fcr_cut
		break
h_fcore_bkg = TH1F("h_fcore_bkg","",nbin_fc,0,1)#lower limit was 0.2 I don't remember why so I changed it back to 0
bkgtree.Draw("m_fcore>>h_fcore_bkg","clus_pt >"+str(cut)+"000")
count = 0.;
countfc = 0.;
countfc95 = 0.
for m in range(bkgtree.GetEntries()):
	bkgtree.GetEntry(m);
	nsds = []
	tcs = []
	for r in range(len(bkgtree.clus_pt)):
		pt = bkgtree.clus_pt[r]/1000.
		tcs.append(pt)
		h_TCmb.Fill(pt)
		if(pt > float(cut)):
			nsds.append(pt)
	tcs.sort()
	tcs.reverse()
	#print len(nsds)
	if(len(nsds)) > 0:
		count = count + 1;
	if(len(tcs)==0):
		continue;
	h_pass.Fill(tcs[0])


for q in range(nsteps):
	left = h_pass.Integral(stepsize*q+2,nbins+1)
	#tot = h_pass.Integral(1,nbins+1)
	rate = (left/events)*coll
	rate2 = ((left/events)**2)*coll
	print "without",left, events, rate
	h_rate.Fill(stepsize*q,rate)
	h_rate2.Fill(stepsize*q,rate2)
	limit = float(q*1000)
	print "limit", limit
	h_fcore_M_siglp = TH1F("h_fcore_M_siglp","",nbin_fc,0,1)
	for entry in range(sigtree.GetEntries()):
                sigtree.GetEntry(entry)
                #print "signal", sigtree.bcId
		
                #if sigtree.bcId in bcidomit:
                #        print "here"
                #        continue;
                for seeds in range(len(sigtree.m_fcore_M)):
			pt = sigtree.clus_pt_M[seeds]
			iso = sigtree.m_iso_pt_M[seeds]- sigtree.m_core_pt_M[seeds]
                        nring = ((sigtree.isoeta * sigtree.isophi)-(sigtree.coreeta * sigtree.corephi))
                        nl2 = sigtree.nlayer2_M[seeds]
                        iso_av = iso / nring
                        reduction = iso_av * nl2
			pt_red = (pt-reduction)
                        if pt_red > limit:
                                h_fcore_M_siglp.Fill(sigtree.m_fcore_M[seeds])


	#sigtree.Draw("m_fcore_M>>h_fcore_M_siglp","clus_pt_M>"+str(limit))
	totlp = h_fcore_M_siglp.Integral(1,nbin_fc+1);
	fcr_cutlp = -999.
	fcr_cutlp95 = -999.
	there = 0;
	for i in range(nbin_fc):
		partlp = h_fcore_M_siglp.Integral(1,i+1)
		if(partlp/totlp>0.05 and there ==0):
			fcr_cutlp95=(i*width)
			there = 1
			#print "here"
		if(partlp/totlp > 0.1):
			fcr_cutlp=(i*width)
			#print fcr_cutlp
			break

	#h_passfc=TH1F("h_passfc","",200,0,200)
	h_passfc95=TH1F("h_passfc95","",200,0,200)
	for m in range(bkgtree.GetEntries()):
		bkgtree.GetEntry(m);
		#nsdsfc = []
		nsdsfc95 = []
		tcsfc = []
		#if bkgtree.bcId in bcidomit:
		#	print "here"
		#	continue;
		for z in range(len(bkgtree.clus_pt)):
			pt = bkgtree.clus_pt[z]
			iso = bkgtree.m_iso_pt[z]- bkgtree.m_core_pt[z]
                        nring = ((bkgtree.isoeta * bkgtree.isophi)-(bkgtree.coreeta * bkgtree.corephi))
                        nl2 = bkgtree.nlayer2[z]
                        iso_av = iso / nring
                        reduction = iso_av * nl2
			pt_red = (pt - reduction)/1000.
			tcsfc.append(pt_red)
			#h_TCmbfc.Fill(pt_red)
			#print fcr_cut, bkgtree.m_fcore[r]
			#if(bkgtree.m_fcore[z] > fcr_cutlp):
			#	nsdsfc.append(pt_red)#/1000.)#/1000.)
			if(bkgtree.m_fcore[z] > fcr_cutlp95):
				nsdsfc95.append(pt_red)#/1000.)
		#nsdsfc.sort()
		#nsdsfc.reverse()
		nsdsfc95.sort()
		nsdsfc95.reverse()
		#print "length of pass", len(nsdsfc)
		'''if(len(nsdsfc)) > 0:
			countfc = countfc + 1;
		if(len(nsdsfc)==0):
			continue;
		h_passfc.Fill(nsdsfc[0])
		'''
		if(len(nsdsfc95)) > 0:
                        countfc95 = countfc95 + 1;
                if(len(nsdsfc95)==0):
                        continue;
                h_passfc95.Fill(nsdsfc95[0])

	'''leftfc90 = h_passfc.Integral(stepsize*q+2,nbins+1)
	ratefc90 = (leftfc90/events)*coll
	ratefc902 = ((leftfc90/events)**2)*coll
	print "with 90", leftfc90,events,ratefc90
	'''
	leftfc95 = h_passfc95.Integral(stepsize*q+2,nbins+1)
	ratefc95 = (leftfc95/events)*coll
	ratefc952 = ((leftfc95/events)**2)*coll
	print "with 95", leftfc95,events,ratefc95
	if(q>9):	
		h_ratefc95.Fill(stepsize*q,ratefc95)
		h_ratefc952.Fill(stepsize*q,ratefc952)
		#h_ratefc90.Fill(stepsize*q,ratefc90)
		#h_ratefc902.Fill(stepsize*q,ratefc902)
	
	h_fcore_M_siglp.Delete()	
	#h_passfc.Delete()
	h_passfc95.Delete()
	if(leftfc95==0):
		continue
	#h_rej.Fill(stepsize*q,leftfc90/left)
	rejout[0] = (leftfc95/left)
	cutout[0] = (stepsize*q)
	out.Fill()

output.cd()
##################################################################
params1D(h_fcore_bkg)
h_fcore_bkg.SetXTitle("F_{Core}")
h_fcore_bkg.SetYTitle("Events")
h_fcore_bkg.Write()

params1D(h_fcore_M_sig)
h_fcore_M_sig.SetXTitle("F_{Core}")
h_fcore_M_sig.SetYTitle("Events")
h_fcore_M_sig.Write()

params1D(h_TCmb)
h_TCmb.SetXTitle("TauCluster Energy")
h_TCmb.SetYTitle("Events")
h_TCmb.Write()

params1D(h_TCh)
h_TCh.SetXTitle("TauCluster Energy")
h_TCh.SetYTitle("Events")
h_TCh.Write()

params1D(h_rate)
h_rate.SetMarkerColor(kBlack)
h_rate.SetMarkerSize(1)
h_rate.SetMarkerStyle(33)
h_rate.SetXTitle("TauCluster Threshold [GeV]")
h_rate.SetYTitle("Single Tau Rate(Hz)")
h_rate.Write()

params1D(h_rate2)
h_rate2.SetMarkerColor(kBlack)
h_rate2.SetMarkerSize(1)
h_rate2.SetMarkerStyle(33)
h_rate2.SetXTitle("TauCluster Threshold [GeV]")
h_rate2.SetYTitle("DiTau Rate(Hz)")
h_rate2.Write()

'''params1D(h_ratefc90)
h_ratefc90.SetMarkerStyle(22)
h_ratefc90.SetMarkerSize(1)
h_ratefc90.SetMarkerColor(kRed)
h_ratefc90.SetXTitle("TauCluster Threshold [GeV]")
h_ratefc90.SetYTitle("Single Tau Rate(Hz)")
h_ratefc90.Write()

params1D(h_ratefc902)
h_ratefc902.SetMarkerStyle(22)
h_ratefc902.SetMarkerSize(1)
h_ratefc902.SetMarkerColor(kRed)
h_ratefc902.SetXTitle("TauCluster Threshold [GeV]")
h_ratefc902.SetYTitle("DiTau Rate(Hz)")
h_ratefc902.Write()
'''
params1D(h_ratefc95)
h_ratefc95.SetMarkerStyle(34)
h_ratefc95.SetMarkerSize(1)
h_ratefc95.SetMarkerColor(kGreen)
h_ratefc95.SetXTitle("TauCluster Threshold [GeV]")
h_ratefc95.SetYTitle("Single Tau Rate(Hz)")
h_ratefc95.Write()

params1D(h_ratefc952)
h_ratefc952.SetMarkerStyle(34)
h_ratefc952.SetMarkerSize(1)
h_ratefc952.SetMarkerColor(kGreen)
h_ratefc952.SetXTitle("TauCluster Threshold [GeV]")
h_ratefc952.SetYTitle("DiTau Rate(Hz)")
h_ratefc952.Write()

'''params1D(h_rej)
h_rej.SetMarkerSize(1)
h_rej.SetMarkerStyle(29)
h_rej.SetYTitle("F_{core} Acceptance")
h_rej.SetXTitle("TC Threshold [GeV]")
h_rej.Write()
'''
h_shit = TH1F("h_shit","",10,0,1)
h_shit.SetLineColor(kBlack)
leg = TLegend(0.5,0.7,0.9,0.9)
leg.AddEntry(h_rate,"No F_{core} cut","p")
leg.AddEntry(h_ratefc95,"95% Tau Efficiency F_{core} cut","p")
leg2 = TLegend(0.5,0.7,0.9,0.9)
leg2.AddEntry(h_rate,"No F_{core} cut","p")
leg2.AddEntry(h_ratefc95,"95% Tau Efficiency F_{core} cut","p")
leg2.AddEntry(h_shit,"200kHz Level 1 Trigger Rate","l")
val = TLine(10,200000,50,200000)
val.SetLineWidth(1)

c1 = TCanvas("c1","",800,600)
c1.cd()
c1.SetLogy(1)
h_rate.Draw("PHISTO")
h_ratefc95.Draw("PHISTOSAME")
#val.Draw("SAME")
leg.Draw()
c1.Print("rate_200_red_"+str(region)+".png")
'''c2 = TCanvas("c2","",800,600)
c2.cd()
c2.SetLogy(0)
h_rej.Draw("PHISTO")
c2.Print("acceptance_200"+str(region)+".png")'''
c3 = TCanvas("c3","",800,600)
c3.cd()
c3.SetLogy(1)
h_rate2.Draw("PHISTO")
h_ratefc952.Draw("PHISTOSAME")
val.Draw("SAME")
leg2.Draw()
c3.Print("rate2_200_red_"+str(region)+".png")
output.Write()
output.Close()
#code.interact(local=locals())
