from ROOT import *
import code
import os
f = TFile("rateplots/output200_115_32.root")
t = f.Get("mytree")
t.AddFriend("mytree","rateplots/output200_54_52.root")
t.AddFriend("mytree","rateplots/output200_73_31.root")
t.AddFriend("mytree","rateplots/output200_95_32.root")

h11 = TH1F("h11","",40,10,50)
h9 = TH1F("h9","",40,10,50)
h7 = TH1F("h7","",40,10,50)
h5 = TH1F("h5","",40,10,50)

for q in range(t.GetEntries()):
	t.GetEntry(q)
	print q

	h11.Fill(t.cutout115_32,t.rejout115_32)
	h9.Fill(t.cutout95_32,t.rejout95_32)
	h7.Fill(t.cutout73_31,t.rejout73_31)
	h5.Fill(t.cutout54_52,t.rejout54_52)

h11.SetMarkerStyle(28)
h11.SetMarkerColor(kRed)

h9.SetMarkerStyle(24)
h9.SetMarkerColor(kBlack)

h7.SetMarkerStyle(25)
h7.SetMarkerColor(kGreen+10)

h5.SetMarkerStyle(27)
h5.SetMarkerColor(kBlue+2)
gStyle.SetOptStat(0)
leg = TLegend(0.25,0.7,0.5,0.9)
leg.SetHeader("Isolation and Core Region (#eta x #phi)")
leg.AddEntry(h11,"11x5 3x2","p")
leg.AddEntry(h9,"9x5 3x2","p")
leg.AddEntry(h7,"7x3 3x1","p")
leg.AddEntry(h5,"5x4 5x2","p")
c1 = TCanvas("c1","",800,600)
h11.SetMaximum(1)
h11.SetXTitle("Tau Cluster Threshold [GeV]")
h11.SetYTitle("F_{Core} Background Acceptance")

h11.Draw("PHISTO")
h9.Draw("PHISTOSAME")
h7.Draw("PHISTOSAME")
h5.Draw("PHISTOSAME")
leg.Draw()
c1.Print("acceptance_all.png")
code.interact(local=locals())
                                
