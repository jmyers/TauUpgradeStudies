import code
import ROOT
from ROOT import *
import os
from os import listdir
import glob
from array import array
import sys
from math import sqrt, pow
#print "f"
region = "113_91"
#mk = 22
#clr = EColor(kBlack)
#region = sys.argv[1]
cut = sys.argv[1] 
directory = "/export/home/jmyers10/Tau_analysis/EventLoopTutorial/OutputGarbage/"

output = TFile("rateplots/gbg_"+str(cut)+".root","RECREATE")
gridtree = TTree("gridtree","gridtree")
rej =array('f',[0.])
isoe = array('i',[0])
isop = array('i',[0])
coree = array('i',[0])
corep = array('i',[0])
rate = array('f',[0.])

gridtree.Branch('rej',rej,'rej/F')
gridtree.Branch('rate',rate,'rate/F')
gridtree.Branch('isoe',isoe,'isoe/I')
gridtree.Branch('isop',isop,'isop/I')
gridtree.Branch('coree',coree,'coree/I')
gridtree.Branch('corep',corep,'corep/I')
for filename in os.listdir(directory):
	print filename
	sigfile = TFile(glob.glob(directory+filename+"/*Z*")[0])
	bkgfile = TFile(glob.glob(directory+filename+"/*MB*")[0])
	sigtree = sigfile.Get("mytree")
	bkgtree = bkgfile.Get("mytree")
	events = bkgtree.GetEntries()
	coll = 4.E7
	frac = coll/events
	nbin_fc = 1000;
	nbin_R = 60;
	width = 1./1000.
	h_fcore_M_sig = TH1F("h_fcore_M_sig","",nbin_fc,0,1)
	sigtree.Draw("m_fcore_M>>h_fcore_M_sig","clus_pt_M >"+str(cut)+"000")
	tot = h_fcore_M_sig.Integral(1,nbin_fc+1);
	for i in range(nbin_fc):
		part = h_fcore_M_sig.Integral(1,i+1)
		if(part/tot > 0.05):
			fcr_cut=(i*width)
			break
	h_fcore_bkg = TH1F("h_fcore_bkg","",nbin_fc,0.2,1)
	for event in range(bkgtree.GetEntries()):
		numsd = 0
		bkgtree.GetEntry(event)
		for sd in range(len(bkgtree.clus_pt)):
			if bkgtree.clus_pt[sd] > float(cut)*1000:
				print "cluspt", bkgtree.clus_pt[sd], bkgtree.m_seed_eta[sd], bkgtree.m_seed_phi[sd]
				numsd = numsd + 1
		print "number of seeds", numsd
		
	bkgtree.Draw("m_fcore>>h_fcore_bkg","clus_pt >"+str(cut)+"000")		
	tot_bkg = h_fcore_bkg.Integral(1,nbin_fc+1)
	cutbin = int(fcr_cut/width)
	part_bkg = h_fcore_bkg.Integral(1,cutbin+1)
	left_ovr = h_fcore_bkg.Integral(cutbin+2,nbin_fc+1)
	v_rate = (left_ovr/tot_bkg**2) * frac
	#v_rate = left_ovr/tot_bkg
	v_rej = part_bkg/tot_bkg
	print part_bkg, tot_bkg
	rate[0]= (v_rate)
	rej[0] = (v_rej)
	print v_rate, v_rej
	sigtree.GetEntry(0)
	v_isoe = sigtree.isoeta
	v_isop = sigtree.isophi
	v_coree = sigtree.coreeta
	v_corep = sigtree.corephi
	print v_isoe, v_isop, v_coree, v_corep
	isoe[0] = (v_isoe)
	isop[0] = (v_isop)
	coree[0] = (v_coree)
	corep[0] = (v_corep)
	output.cd()
	gridtree.Fill()
output.cd()
output.Write()
output.Close()

#print rate, rej
##################################################################

#code.interact(local=locals())
